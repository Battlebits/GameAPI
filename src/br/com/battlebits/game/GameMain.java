package br.com.battlebits.game;

import br.com.battlebits.commons.bukkit.command.BukkitCommandFramework;
import br.com.battlebits.commons.core.backend.mongodb.MongoBackend;
import br.com.battlebits.commons.core.command.CommandLoader;
import br.com.battlebits.commons.core.translate.T;
import br.com.battlebits.commons.core.translate.Translate;
import br.com.battlebits.commons.util.updater.AutoUpdater;
import br.com.battlebits.game.constructor.Gamer;
import br.com.battlebits.game.event.game.GameStageChangeEvent;
import br.com.battlebits.game.event.game.GameStartEvent;
import br.com.battlebits.game.event.game.GameTimerEvent;
import br.com.battlebits.game.games.hungergames.HungerGamesMode;
import br.com.battlebits.game.listener.*;
import br.com.battlebits.game.manager.*;
import br.com.battlebits.game.scheduler.SchedulerListener;
import br.com.battlebits.game.serverinfo.ServerInfoInjector;
import br.com.battlebits.game.stage.CounterType;
import br.com.battlebits.game.stage.GameStage;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class GameMain extends JavaPlugin {

	private int timer;
	private CounterType timerType = CounterType.STOP;
	private int minimumPlayers = 5;
	private int totalPlayers;
	private GameType gameType = GameType.NONE;
	private GameStage gameStage = GameStage.NONE;

	private GameMode gameMode;

	// MANAGERS
	private AbilityManager abilityManager;
	private CooldownManager cooldownManager;
	private GameEventManager gameEventManager = new GameEventManager();
	private GamerManager gamerManager = new GamerManager();
	private KitManager kitManager;
	private SchedulerManager schedulerManager = new SchedulerManager(this);

	private String database = "";
	private String password = "5^SM`.~MCxuZyU3a";

	// Mongo
	@Getter
	private MongoBackend mongoBackend;
	private String mongoHostname = "127.0.0.1";
	private String mongoDatabase = "gameapi";
	private String mongoUsername = "gameapi";
	private String mongoPassword = "";
	private int mongoPort = 27017;


	private static GameMain plugin;

	{
		plugin = this;
	}

	@Override
	public void onLoad() {
		new AutoUpdater(this, "']uV{65Ut$Ba+}Z7").run();
		loadGamemode();
		getLogger().info("Gamemode carregado > " + gameType.toString());
		gameMode.onLoad();
		database = gameType.toString().toLowerCase();
		ServerInfoInjector.inject(this);
	}

	@Override
	public void onEnable() {
		loadConfiguration();
		try {
			mongoBackend = new MongoBackend(mongoHostname, mongoDatabase, mongoUsername, mongoPassword, mongoPort);
			mongoBackend.startConnection();
		} catch (Exception e) {
			e.printStackTrace();
		}
		Translate t = new Translate(gameType.toString().toLowerCase(), mongoBackend);
		t.loadTranslations();
		T.loadTranslate(this, t);
		this.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
		new CommandLoader(new BukkitCommandFramework(this)).loadCommandsFromPackage("br.com.battlebits.game.command");
		loadListeners();
		gameMode.onEnable();
		cooldownManager = new CooldownManager(this);
		abilityManager = new AbilityManager();
		kitManager = new KitManager();
		totalPlayers = Bukkit.getMaxPlayers();
	}

	@Override
	public void onDisable() {
		gameMode.onDisable();
	}

	public void startGame() {
		gameMode.startGame();
		abilityManager.registerAbilityListeners();
		getServer().getPluginManager().callEvent(new GameStartEvent());
		totalPlayers = playersLeft();
	}

	private void loadGamemode() {
		gameMode = new HungerGamesMode(this);// new PvPTournament(this); //
		gameType = gameMode.getGameType();
		database = gameType.name().toLowerCase();
	}

	private void loadConfiguration() {
		saveDefaultConfig();
		mongoHostname = getConfig().getString("mongo.hostname", "localhost");
		mongoPort = getConfig().getInt("mongo.port", 27017);
		mongoDatabase = getConfig().getString("mongo.database", "");
		mongoUsername = getConfig().getString("mongo.username", "");
		mongoPassword = getConfig().getString("mongo.password", "");
	}

	private void loadListeners() {
		getServer().getPluginManager().registerEvents(new DamagerFixer(), this);
		getServer().getPluginManager().registerEvents(new SchedulerListener(this), this);
		getServer().getPluginManager().registerEvents(new EventListener(this), this);
		getServer().getPluginManager().registerEvents(new JoinListener(this), this);
		getServer().getPluginManager().registerEvents(new QuitListener(this), this);
		getServer().getPluginManager().registerEvents(new SpectatorListener(), this);
	}

	public GameType getGameType() {
		return gameType;
	}

	public GameStage getGameStage() {
		return gameStage;
	}

	public int getTotalPlayers() {
		return totalPlayers;
	}

	public void setGameStage(GameStage gameStage) {
		getServer().getPluginManager().callEvent(new GameStageChangeEvent(this.gameStage, gameStage));
		this.gameStage = gameStage;
		setTimerType(getGameStage().getDefaultType());
		setTimer(getGameStage().getDefaultTimer());
	}

	public void setMinimumPlayers(int minimumPlayers) {
		this.minimumPlayers = minimumPlayers;
	}

	public GameMode getGameMode() {
		return gameMode;
	}

	public ScoreboardManager getScoreboardManager() {
		return gameMode.getScoreBoardManager();
	}

	public AbilityManager getAbilityManager() {
		return abilityManager;
	}

	public CooldownManager getCooldownManager() {
		return cooldownManager;
	}

	public GameEventManager getGameEventManager() {
		return gameEventManager;
	}

	public GamerManager getGamerManager() {
		return gamerManager;
	}

	public KitManager getKitManager() {
		return kitManager;
	}

	public SchedulerManager getSchedulerManager() {
		return schedulerManager;
	}

	public int getTimer() {
		return timer;
	}

	public void setTimer(int timer) {
		getServer().getPluginManager().callEvent(new GameTimerEvent());
		this.timer = timer;
	}

	public void setTimerType(CounterType timerType) {
		this.timerType = timerType;
	}

	public void count() {
		switch (timerType) {
		case COUNTDOWN:
			setTimer(timer - 1);
			break;
		case COUNT_UP:
			setTimer(timer + 1);
			break;
		default:
			break;

		}
	}

	public void checkTimer() {
		int i = minimumPlayers;
		for (Player p : Bukkit.getOnlinePlayers()) {
			Gamer gamer = Gamer.getGamer(p);
			if (gamer == null)
				continue;
			if (gamer.isGamemaker())
				continue;
			if (gamer.isSpectator())
				continue;
			i--;
		}
		if (getGameStage() == GameStage.WAITING) {
			if (i <= 0) {
				setGameStage(GameStage.PREGAME);
			}
		} else if (getGameStage() == GameStage.PREGAME || getGameStage() == GameStage.STARTING) {
			if (i > 0) {
				setGameStage(GameStage.WAITING);
				setTimer(GameStage.PREGAME.getDefaultTimer());
			}
		}
	}

	public int playersLeft() {
		int i = 0;
		for (Player p : getServer().getOnlinePlayers()) {
			Gamer gamer = getGamerManager().getGamer(p.getUniqueId());
			if (gamer == null)
				continue;
			if (gamer.isGamemaker())
				continue;
			if (gamer.isSpectator())
				continue;
			if (!p.isOnline())
				continue;
			i++;
		}
		return i;
	}

	public static GameMain getPlugin() {
		return plugin;
	}

}
