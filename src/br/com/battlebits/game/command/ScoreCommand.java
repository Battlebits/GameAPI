package br.com.battlebits.game.command;

import br.com.battlebits.commons.core.translate.T;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;

import br.com.battlebits.commons.bukkit.command.BukkitCommandArgs;
import br.com.battlebits.commons.core.account.BattlePlayer;
import br.com.battlebits.commons.core.command.CommandClass;
import br.com.battlebits.commons.core.command.CommandFramework.Command;
import br.com.battlebits.commons.core.translate.Translate;
import br.com.battlebits.game.GameMain;
import br.com.battlebits.game.constructor.Gamer;

public class ScoreCommand implements CommandClass {

	@Command(name = "score", aliases = { "scoreboard" })
	public void admin(BukkitCommandArgs args) {
		if (args.isPlayer()) {
			Player p = args.getPlayer();
			Gamer gamer = GameMain.getPlugin().getGamerManager().getGamer(p.getUniqueId());

			if (gamer.isScoreboardEnabled()) {
				p.getScoreboard().getObjective("clear").setDisplaySlot(DisplaySlot.SIDEBAR);
				p.sendMessage(
						T.t(GameMain.getPlugin(), BattlePlayer.getLanguage(p.getUniqueId()), "scoreboard-disabled"));
				gamer.setScoreboardEnabled(false);
			} else {
				GameMain.getPlugin().getScoreboardManager().enableScoreboard(p);
				p.sendMessage(
						T.t(GameMain.getPlugin(), BattlePlayer.getLanguage(p.getUniqueId()), "scoreboard-enabled"));
				gamer.setScoreboardEnabled(true);
			}
		}
	}

}
