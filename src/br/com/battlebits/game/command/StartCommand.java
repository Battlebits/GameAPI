package br.com.battlebits.game.command;

import br.com.battlebits.commons.core.account.BattlePlayer;
import br.com.battlebits.commons.core.command.CommandArgs;
import br.com.battlebits.commons.core.command.CommandClass;
import br.com.battlebits.commons.core.command.CommandFramework.Command;
import br.com.battlebits.commons.core.permission.Group;
import br.com.battlebits.commons.core.translate.Language;
import br.com.battlebits.commons.core.translate.T;
import br.com.battlebits.commons.core.translate.Translate;
import br.com.battlebits.game.GameMain;

public class StartCommand implements CommandClass {

	@Command(name = "start", aliases = {
			"comecar" }, groupToUse = Group.MOD, noPermMessageId = "command-start-no-access")
	public void time(CommandArgs args) {
		Language lang = BattlePlayer.getLanguage(args.getSender().getUniqueId());
		args.getSender().sendMessage(T.t(GameMain.getPlugin(), lang, "command-start-started"));
		GameMain.getPlugin().startGame();
	}

}