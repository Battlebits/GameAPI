package br.com.battlebits.game.command;

import br.com.battlebits.commons.core.account.BattlePlayer;
import br.com.battlebits.commons.core.command.CommandArgs;
import br.com.battlebits.commons.core.command.CommandClass;
import br.com.battlebits.commons.core.command.CommandFramework.Command;
import br.com.battlebits.commons.core.permission.Group;
import br.com.battlebits.commons.core.translate.Language;
import br.com.battlebits.commons.core.translate.T;
import br.com.battlebits.commons.core.translate.Translate;
import br.com.battlebits.commons.util.DateUtils;
import br.com.battlebits.game.GameMain;

public class TimeCommand implements CommandClass {

	@Command(name = "time", aliases = { "tempo" }, groupToUse = Group.MOD, noPermMessageId = "command-time-no-access")
	public void time(CommandArgs args) {
		Language lang = BattlePlayer.getLanguage(args.getSender().getUniqueId());
		String prefix = T.t(GameMain.getPlugin(), lang, "command-time-prefix") + " ";
		if (args.getArgs().length != 1) {
			args.getSender().sendMessage(prefix + T.t(GameMain.getPlugin(), lang, "command-time-usage").replace("%command%", args.getLabel()));
			return;
		}
		long time;
		try {
			time = DateUtils.parseDateDiff(args.getArgs()[0], true);
		} catch (Exception e) {
			args.getSender().sendMessage(prefix + T.t(GameMain.getPlugin(), lang, "invalid-format"));
			return;
		}
		int seconds = (int) Math.floor((time - System.currentTimeMillis()) / 1000);
		GameMain.getPlugin().setTimer(seconds);
		args.getSender().sendMessage(prefix + T.t(GameMain.getPlugin(), lang, "command-time-changed").replace("%time%", DateUtils.formatDifference(lang, seconds)).replace("%stage%", GameMain.getPlugin().getGameStage().toString()));
	}

}