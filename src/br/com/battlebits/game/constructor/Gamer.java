package br.com.battlebits.game.constructor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import br.com.battlebits.commons.core.translate.T;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import br.com.battlebits.commons.core.account.BattlePlayer;
import br.com.battlebits.commons.core.translate.Translate;
import br.com.battlebits.game.GameMain;
import br.com.battlebits.game.constructor.gamer.GamerType;
import br.com.battlebits.game.data.DataGamer;
import br.com.battlebits.game.event.player.PlayerSpectateEvent;

public class Gamer {
	private UUID uniqueId;
	private HashMap<CustomKitType, HashMap<String, CustomKit>> customKits = new HashMap<>();
	private Set<String> kitsOwned = new HashSet<>();
	private HashMap<String, Integer> abilitiesOwned = new HashMap<>();
	private List<String> kitsFavoritos = new ArrayList<>();
	private int customKitSlots = 1;
	private int wins = 0;
	private int kills = 0;
	private transient int matchkills = 0;
	private int deaths = 0;
	private GamerType gamerType = GamerType.BETA_TESTER;
	private transient int multiKill = 0;
	private transient long lastKill = Long.MIN_VALUE;
	private transient boolean scoreboardEnabled = true;
	private transient boolean isSpectator = false;
	private transient boolean isGamemaker = false;
	private transient boolean spectatorsEnabled = false;

	public Gamer(UUID uuid) {
		this.uniqueId = uuid;
	}

	public UUID getUniqueId() {
		return uniqueId;
	}

	public Player getPlayer() {
		return Bukkit.getPlayer(uniqueId);
	}

	public void setScoreboardEnabled(boolean scoreboardEnabled) {
		this.scoreboardEnabled = scoreboardEnabled;
	}

	public void setGamemaker(boolean isGamemaker) {
		this.isGamemaker = isGamemaker;
	}

	public void setSpectator(boolean isSpectator) {
		this.isSpectator = isSpectator;
		if (isSpectator) {
			PlayerSpectateEvent event = new PlayerSpectateEvent(getPlayer());
			Bukkit.getPluginManager().callEvent(event);
		}
	}

	public int getCustomKitSlots() {
		return customKitSlots;
	}

	public Set<String> getKitsOwned() {
		return kitsOwned;
	}

	public GamerType getGamerType() {
		return gamerType;
	}

	public List<String> getKitsFavoritos() {
		return kitsFavoritos;
	}

	public HashMap<String, Integer> getAbilitiesOwned() {
		return abilitiesOwned;
	}

	public List<CustomKit> getAllCustomKits() {
		List<CustomKit> customKits = new ArrayList<>();
		customKits.addAll(getYourCustomKits().values());
		customKits.addAll(getVipCustomKits().values());
		customKits.addAll(getFullCustomKits().values());
		return customKits;
	}

	public HashMap<CustomKitType, HashMap<String, CustomKit>> getCustomKits() {
		return customKits;
	}

	public HashMap<String, CustomKit> getYourCustomKits() {
		if (!customKits.containsKey(CustomKitType.NORMAL)) {
			customKits.put(CustomKitType.NORMAL, new HashMap<>());
		}
		return customKits.get(CustomKitType.NORMAL);
	}

	public HashMap<String, CustomKit> getVipCustomKits() {
		if (!customKits.containsKey(CustomKitType.VIP)) {
			customKits.put(CustomKitType.VIP, new HashMap<>());
		}
		return customKits.get(CustomKitType.VIP);
	}

	public HashMap<String, CustomKit> getFullCustomKits() {
		if (!customKits.containsKey(CustomKitType.FULL)) {
			customKits.put(CustomKitType.FULL, new HashMap<>());
		}
		return customKits.get(CustomKitType.FULL);
	}

	public CustomKit getCustomKit(String name) {
		name = name.toLowerCase();
		for (CustomKitType type : CustomKitType.values()) {
			if (!customKits.containsKey(type))
				continue;
			if (customKits.get(type).containsKey(name))
				return customKits.get(type).get(name);
		}
		return null;
	}

	public boolean hasCustomKitName(String name) {
		name = name.toLowerCase();
		for (CustomKitType type : CustomKitType.values()) {
			if (!customKits.containsKey(type))
				continue;
			if (customKits.get(type).containsKey(name))
				return true;
		}
		return false;
	}

	private static transient boolean allAbilityFree = true;
	private static transient boolean allKitFree = true;
	public boolean hasAbility(Ability ability) {
		if (allAbilityFree)
			return allAbilityFree;
		return abilitiesOwned.containsKey(ability.getName().toLowerCase())
				&& abilitiesOwned.get(ability.getName().toLowerCase()) > 0;
	}

	public boolean hasKit(String kitName) {
		if (allKitFree)
			return allKitFree;
		return kitsOwned.contains(kitName.toLowerCase());
	}

	public void addKill() {
		addKill(1);
	}

	public void addKill(int i) {
		setKills(kills + i);
		matchkills += i;
	}

	public void addDeath() {
		addDeath(1);
	}

	public void addDeath(int i) {
		setDeaths(deaths + i);
	}

	public void addWin() {
		addDeath(1);
	}

	public void addWin(int i) {
		setWins(wins + i);
	}

	public void setKills(int kills) {
		this.kills = kills;
		DataGamer.saveGamerField(this, "kills");
	}

	public void setDeaths(int deaths) {
		this.deaths = deaths;
		DataGamer.saveGamerField(this, "deaths");
	}

	public void setWins(int wins) {
		this.wins = wins;
		DataGamer.saveGamerField(this, "wins");
	}

	public int getKills() {
		return kills;
	}

	public int getMatchkills() {
		return matchkills;
	}

	public int getDeaths() {
		return deaths;
	}

	public int getWins() {
		return wins;
	}

	public int getMultiKill() {
		return multiKill;
	}

	public String getKitName() {
		return getKit() != null ? getKit().getName()
				: T.t(GameMain.getPlugin(), BattlePlayer.getLanguage(getUniqueId()), "none-kit");
	}

	public Kit getKit() {
		return GameMain.getPlugin().getKitManager().getPlayerKit(uniqueId);
	}

	public long getLastKill() {
		return lastKill;
	}

	public boolean isScoreboardEnabled() {
		return scoreboardEnabled;
	}

	public boolean isGamemaker() {
		return isGamemaker;
	}

	public boolean isSpectator() {
		return isSpectator;
	}

	public boolean isSpectatorsEnabled() {
		return spectatorsEnabled;
	}

	public boolean isNotPlaying() {
		return isGamemaker || isSpectator;
	}

	public static Gamer getGamer(Player player) {
		return getGamer(player.getUniqueId());
	}

	public static Gamer getGamer(UUID uuid) {
		return GameMain.getPlugin().getGamerManager().getGamer(uuid);
	}

	public static enum CustomKitType {
		NORMAL, VIP, FULL;
	}
}
