package br.com.battlebits.game.constructor;

import br.com.battlebits.game.GameType;
import br.com.battlebits.game.stage.GameStage;

public class ScheduleArgs {
	private GameType gameType;
	private GameStage stage;
	private int timer;

	public ScheduleArgs(GameType gameType, GameStage stage, int timer) {
		this.gameType = gameType;
		this.stage = stage;
		this.timer = timer;
	}

	public GameType getGameType() {
		return gameType;
	}

	public GameStage getStage() {
		return stage;
	}

	public int getTimer() {
		return timer;
	}

}
