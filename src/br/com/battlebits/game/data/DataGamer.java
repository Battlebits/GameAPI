package br.com.battlebits.game.data;

import java.util.UUID;

import br.com.battlebits.game.GameMain;
import org.bson.Document;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;

import br.com.battlebits.commons.BattlebitsAPI;
import br.com.battlebits.game.constructor.Gamer;

public class DataGamer {

	public static Gamer getGamer(UUID uuid) {
		MongoDatabase database = GameMain.getPlugin().getMongoBackend().getClient().getDatabase("hungergames");
		MongoCollection<Document> collection = database.getCollection("gamer");

		Document found = collection.find(Filters.eq("uniqueId", uuid.toString())).first();
		if (found == null) {
			return null;
		}
		return BattlebitsAPI.getGson().fromJson(BattlebitsAPI.getGson().toJson(found), Gamer.class);
	}

	public static Gamer createIfNotExistMongo(UUID uuid) {
		MongoDatabase database = GameMain.getPlugin().getMongoBackend().getClient().getDatabase("hungergames");
		MongoCollection<Document> collection = database.getCollection("gamer");

		Document found = collection.find(Filters.eq("uniqueId", uuid.toString())).first();
		Gamer gamer = null;
		if (found == null) {
			gamer = new Gamer(uuid);
			found = Document.parse(BattlebitsAPI.getGson().toJson(gamer));
			collection.insertOne(found);
		} else {
			gamer = BattlebitsAPI.getGson().fromJson(BattlebitsAPI.getGson().toJson(found), Gamer.class);
		}
		return gamer;
	}

	public static void saveGamerField(Gamer gamer, String fieldName) {
		JsonObject jsonObject = BattlebitsAPI.getParser().parse(BattlebitsAPI.getGson().toJson(gamer))
				.getAsJsonObject();
		if (!jsonObject.has(fieldName))
			return;
		JsonElement element = jsonObject.get(fieldName);

		MongoDatabase database = GameMain.getPlugin().getMongoBackend().getClient().getDatabase("hungergames");
		MongoCollection<Document> collection = database.getCollection("gamer");
		collection.updateOne(Filters.eq("uniqueId", gamer.getUniqueId().toString()),
				new Document("$set", new Document(fieldName, (element.isJsonObject()
						? Document.parse(element.getAsJsonObject().toString()) : element.getAsString()))));
	}
}
