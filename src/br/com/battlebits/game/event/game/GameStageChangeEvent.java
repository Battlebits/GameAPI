package br.com.battlebits.game.event.game;

import br.com.battlebits.game.event.Event;
import br.com.battlebits.game.stage.GameStage;

public class GameStageChangeEvent extends Event {
	private GameStage lastStage;
	private GameStage newStage;

	public GameStageChangeEvent(GameStage lastStage, GameStage newStage) {
		this.lastStage = lastStage;
		this.newStage = newStage;
	}

	public GameStage getNewStage() {
		return newStage;
	}

	public GameStage getLastStage() {
		return lastStage;
	}

}
