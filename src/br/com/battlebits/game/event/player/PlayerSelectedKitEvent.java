package br.com.battlebits.game.event.player;

import org.bukkit.entity.Player;

import br.com.battlebits.game.constructor.Kit;
import br.com.battlebits.game.event.Event;

public class PlayerSelectedKitEvent extends Event {

	private Player player;
	private Kit kit;

	public PlayerSelectedKitEvent(Player player, Kit kit) {
		this.player = player;
		this.kit = kit;
	}

	public Player getPlayer() {
		return player;
	}

	public Kit getKit() {
		return kit;
	}

}
