package br.com.battlebits.game.event.player;

import org.bukkit.entity.Player;

import br.com.battlebits.game.event.Event;

public class PlayerSpectateEvent extends Event {

	private Player player;
	
	public PlayerSpectateEvent(Player player) {
		this.player = player;
	}
	
	public Player getPlayer() {
		return player;
	}
	
}
