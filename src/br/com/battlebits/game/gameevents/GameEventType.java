package br.com.battlebits.game.gameevents;

public enum GameEventType {
	SERVER_START, COUNTER_START, GAME_START, INVINCIBILITY_WORN_OFF, KILL, DEATH, PLAYER_JOIN, PLAYER_LEAVE, CUSTOM, WIN
}
