package br.com.battlebits.game.gameevents.gamer;

import java.util.UUID;

import br.com.battlebits.game.gameevents.GameEventType;

public class GamerQuitEvent extends GamerEvent {

	public GamerQuitEvent(UUID uuid) {
		super(uuid, GameEventType.PLAYER_LEAVE.toString().toLowerCase(), GameEventType.PLAYER_LEAVE);
	}

}
