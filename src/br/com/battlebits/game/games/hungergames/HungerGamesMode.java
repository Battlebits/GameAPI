package br.com.battlebits.game.games.hungergames;

import java.util.Random;

import br.com.battlebits.commons.core.translate.T;
import org.bukkit.Bukkit;
import org.bukkit.Difficulty;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import br.com.battlebits.commons.bukkit.command.BukkitCommandFramework;
import br.com.battlebits.commons.core.account.BattlePlayer;
import br.com.battlebits.commons.core.command.CommandLoader;
import br.com.battlebits.commons.core.data.DataServer;
import br.com.battlebits.commons.core.server.loadbalancer.server.MinigameState;
import br.com.battlebits.commons.core.translate.Translate;
import br.com.battlebits.game.GameMain;
import br.com.battlebits.game.GameMode;
import br.com.battlebits.game.GameType;
import br.com.battlebits.game.constructor.Ability;
import br.com.battlebits.game.constructor.Gamer;
import br.com.battlebits.game.constructor.Kit;
import br.com.battlebits.game.constructor.ScheduleArgs;
import br.com.battlebits.game.gameevents.GameEventType;
import br.com.battlebits.game.games.hungergames.listener.BorderListener;
import br.com.battlebits.game.games.hungergames.listener.CombatLogListener;
import br.com.battlebits.game.games.hungergames.listener.DeathListener;
import br.com.battlebits.game.games.hungergames.listener.GameListener;
import br.com.battlebits.game.games.hungergames.listener.GameStageChangeListener;
import br.com.battlebits.game.games.hungergames.listener.InventoryListener;
import br.com.battlebits.game.games.hungergames.listener.InvincibilityListener;
import br.com.battlebits.game.games.hungergames.listener.PregameListener;
import br.com.battlebits.game.games.hungergames.listener.ScoreboardListener;
import br.com.battlebits.game.games.hungergames.listener.SelectKitListener;
import br.com.battlebits.game.games.hungergames.listener.SpectatorListener;
import br.com.battlebits.game.games.hungergames.listener.UpdateListener;
import br.com.battlebits.game.games.hungergames.manager.ColiseumManager;
import br.com.battlebits.game.games.hungergames.schedule.InvincibilityScheduler;
import br.com.battlebits.game.games.hungergames.scoreboard.ScoreboardManager;
import br.com.battlebits.game.stage.GameStage;
import br.com.battlebits.game.util.MapUtils;

public class HungerGamesMode extends GameMode {

	public static int MINIMUM_PLAYERS = 5;
	public static int INVINCIBILITY_TIME = 120;
	public static int FEAST_SPAWN = (17 * 60) + 30;
	public static int BONUSFEAST_SPAWN = 30 * 60;
	public static int FINALBATTLE_TIME = 45 * 60;
	public static boolean FINISHED;

	private ScoreboardManager sbManager;

	public HungerGamesMode(GameMain main) {
		super(main, GameType.HUNGERGAMES);
	}

	@Override
	public void onLoad() {
		MapUtils.deleteWorld("world");
		getGameMain().setGameStage(GameStage.WAITING);
		getGameMain().setMinimumPlayers(MINIMUM_PLAYERS);
	}

	@Override
	public void onEnable() {
		getGameMain().getGameEventManager().newEvent(GameEventType.SERVER_START);
		sbManager = new ScoreboardManager(this);
		new CommandLoader(new BukkitCommandFramework(getGameMain()))
				.loadCommandsFromPackage("br.com.battlebits.game.games.hungergames.command");
		loadListeners();
		getGameMain().getServer().getScheduler().scheduleSyncDelayedTask(getGameMain(), new Runnable() {
			public void run() {
				World world = getServer().getWorld("world");
				world.setSpawnLocation(0, getServer().getWorlds().get(0).getHighestBlockYAt(0, 0), 0);
				for (int x = -5; x <= 5; x++)
					for (int z = -5; z <= 5; z++)
						world.getSpawnLocation().clone().add(x * 16, 0, z * 16).getChunk().load();
				world.setDifficulty(Difficulty.NORMAL);
				if (world.hasStorm())
					world.setStorm(false);
				world.setWeatherDuration(999999999);
				world.setGameRuleValue("doDaylightCycle", "false");
				org.bukkit.WorldBorder border = world.getWorldBorder();
				border.setCenter(0, 0);
				border.setSize(1000);
				for (Entity e : world.getEntities()) {
					e.remove();
				}
			}
		});
		new BukkitRunnable() {
			@Override
			public void run() {
				DataServer.updateStatus(MinigameState.WAITING, GameMain.getPlugin().getTimer());
			}
		}.runTaskLaterAsynchronously(getGameMain(), 1);
	}

	@Override
	public void onDisable() {

	}

	private void loadListeners() {
		getGameMain().getServer().getPluginManager().registerEvents(new GameStageChangeListener(getGameMain()),
				getGameMain());
		getGameMain().getServer().getPluginManager().registerEvents(new PregameListener(getGameMain()), getGameMain());
		getGameMain().getServer().getPluginManager().registerEvents(new InventoryListener(getGameMain()),
				getGameMain());
		getGameMain().getServer().getPluginManager().registerEvents(new BorderListener(getGameMain()), getGameMain());
		getGameMain().getServer().getPluginManager().registerEvents(new ScoreboardListener(getGameMain(), this),
				getGameMain());
		getGameMain().getServer().getPluginManager().registerEvents(new SelectKitListener(), getGameMain());
		getGameMain().getServer().getPluginManager().registerEvents(new UpdateListener(getGameMain()), getGameMain());
		getGameMain().getServer().getPluginManager().registerEvents(new SpectatorListener(getGameMain()),
				getGameMain());
	}

	@Override
	public void startGame() {
		Bukkit.broadcastMessage("§%game-started%§");
		getGameMain().getServer().getPluginManager().registerEvents(new GameListener(getGameMain()), getGameMain());
		getGameMain().getServer().getPluginManager().registerEvents(new CombatLogListener(), getGameMain());
		getGameMain().getServer().getPluginManager().registerEvents(new DeathListener(getGameMain(), this),
				getGameMain());
		InvincibilityScheduler scheduler = new InvincibilityScheduler();
		scheduler.pulse(
				new ScheduleArgs(getGameMain().getGameType(), getGameMain().getGameStage(), getGameMain().getTimer()));
		getGameMain().getSchedulerManager().addScheduler("invincibility", scheduler);
		getGameMain().setTimer(INVINCIBILITY_TIME);
		getGameMain().setGameStage(GameStage.INVINCIBILITY);
		getGameMain().getServer().getPluginManager().registerEvents(new InvincibilityListener(getGameMain()),
				getGameMain());
		for (Player p : getGameMain().getServer().getOnlinePlayers()) {
			Gamer gamer = getGameMain().getGamerManager().getGamer(p.getUniqueId());
			if (gamer.isGamemaker())
				continue;
			if (gamer.isSpectator())
				continue;
			if (!ColiseumManager.isInsideColiseum(p))
				teleportToSpawn(p);
			p.playSound(p.getLocation(), Sound.AMBIENCE_THUNDER, 1f, 1f);
			p.playSound(p.getLocation(), Sound.ENDERDRAGON_GROWL, 1f, 1f);
			p.closeInventory();
			p.getInventory().clear();
			p.setGameMode(org.bukkit.GameMode.SURVIVAL);
			Kit playerKit = getGameMain().getKitManager().getPlayerKit(p);
			if (playerKit != null)
				for (Ability ability : playerKit.getAbilities()) {
					ability.giveItems(p);
				}
			p.getInventory().addItem(new ItemStack(Material.COMPASS));
		}
	}

	public void checkWinner() {
		if (getGameMain().playersLeft() > 1) {
			return;
		}
		Player pWin = null;
		for (Player p : Bukkit.getOnlinePlayers()) {
			Gamer gamer = Gamer.getGamer(p);
			if (gamer.isGamemaker())
				continue;
			if (gamer.isSpectator())
				continue;
			if (!p.isOnline())
				continue;
			pWin = p;
			break;
		}
		if (pWin == null) {
			Bukkit.broadcastMessage("§%no-winner-kick%§");
			Bukkit.shutdown();
			return;
		}
		FINISHED = true;
		final Player winner = pWin;
		Gamer win = Gamer.getGamer(winner);
		for (Player player : getGameMain().getServer().getOnlinePlayers()) {
			player.sendMessage(T.t(GameMain.getPlugin(),BattlePlayer.getLanguage(player.getUniqueId()), "player-winner")
					.replace("%player%", winner.getName()).replace("%kills%", win.getMatchkills() + ""));
		}
		new BukkitRunnable() {

			@Override
			public void run() {
				winner.kickPlayer(
						T.t(GameMain.getPlugin(),BattlePlayer.getLanguage(winner.getUniqueId()), "player-winner-kick")
								.replace("%player%", winner.getName()).replace("%kills%", win.getMatchkills() + ""));
				for (Player player : getGameMain().getServer().getOnlinePlayers()) {
					player.kickPlayer(T.t(GameMain.getPlugin(),BattlePlayer.getLanguage(player.getUniqueId()), "other-winner-kick")
							.replace("%player%", winner.getName()).replace("%kills%", win.getMatchkills() + ""));
				}
				Bukkit.shutdown();
			}
		}.runTaskLater(getGameMain(), 20 * 30);
	}

	public static void teleportToSpawn(Player player) {
		player.teleport(getSpawnLocation());
	}

	public static Location getSpawnLocation() {
		Random r = new Random();
		int x = r.nextInt(30);
		int z = r.nextInt(30);
		if (r.nextBoolean())
			x = -x;
		if (r.nextBoolean())
			z = -z;
		World world = Bukkit.getWorlds().get(0);
		int y = world.getHighestBlockYAt(x, z);
		Location loc = new Location(world, x, y + 1, z);
		if (!loc.getChunk().isLoaded()) {
			loc.getChunk().load();
		}
		return loc;
	}

	@Override
	public ScoreboardManager getScoreBoardManager() {
		return sbManager;
	}

}
