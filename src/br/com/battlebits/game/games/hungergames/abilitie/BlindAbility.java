package br.com.battlebits.game.games.hungergames.abilitie;

import java.util.HashMap;
import java.util.Random;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import br.com.battlebits.game.ability.AbilityRarity;
import br.com.battlebits.game.constructor.Ability;
import br.com.battlebits.game.constructor.CustomOption;
import br.com.battlebits.game.interfaces.Disableable;

public class BlindAbility extends Ability implements Disableable {

	public BlindAbility() {
		super(new ItemStack(Material.EGG, 1, (byte) 65), AbilityRarity.RARE);
		options.put("CHANCE", new CustomOption("CHANCE", new ItemStack(Material.GOLD_NUGGET), -1, 1, 3, 5));
		options.put("DURATION", new CustomOption("DURATION", new ItemStack(Material.COAL), 1, 3, 5, 10));
	}

	@EventHandler
	public void onSnail(EntityDamageByEntityEvent event) {
		if (!(event.getEntity() instanceof Player))
			return;
		if (!(event.getDamager() instanceof Player))
			return;
		Player damager = (Player) event.getDamager();
		if (!hasAbility(damager))
			return;
		Random r = new Random();
		Player damaged = (Player) event.getEntity();
		if (damaged instanceof Player) {
			if (r.nextInt(getOption(damager, "CHANCE").getValue()) == 0) {
				damaged.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, getOption(damager, "DURATION").getValue() * 20, 0));
			}
		}
	}

	@Override
	public int getPowerPoints(HashMap<String, CustomOption> map) {
		return getOption("DURATION", map).getValue() * 3 + (18 - (getOption("CHANCE", map).getValue() * 3));
	}
}
