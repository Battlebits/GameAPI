package br.com.battlebits.game.games.hungergames.abilitie;

import java.util.HashMap;

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.BlockIterator;
import org.bukkit.util.Vector;

import br.com.battlebits.game.GameMain;
import br.com.battlebits.game.ability.AbilityRarity;
import br.com.battlebits.game.constructor.Ability;
import br.com.battlebits.game.constructor.CustomOption;
import br.com.battlebits.game.games.hungergames.util.ItemUtils;
import br.com.battlebits.game.interfaces.Disableable;
import net.md_5.bungee.api.ChatColor;

public class BlindbeamAbility extends Ability implements Disableable {

	public BlindbeamAbility() {
		super(new ItemStack(Material.COAL_BLOCK), AbilityRarity.EPIC);
		options.put("ITEM", new CustomOption("ITEM", new ItemStack(Material.EMERALD_BLOCK), ChatColor.BLACK + "Blindness Beam"));
		options.put("COOLDOWN", new CustomOption("COOLDOWN", new ItemStack(Material.WATCH), -1, 15, 30, 60));
		options.put("DURATION", new CustomOption("DURATION", new ItemStack(Material.COAL), 1, 3, 5, 15));
		options.put("DAMAGE", new CustomOption("DAMAGE", new ItemStack(Material.REDSTONE), 1, 1, 4, 8));
	}

	@EventHandler
	public void onSnail(PlayerInteractEvent event) {
		Player p = event.getPlayer();
		if (!hasAbility(p))
			return;
		Action a = event.getAction();
		ItemStack item = p.getItemInHand();
		if (!a.name().contains("RIGHT") && !a.name().contains("LEFT"))
			return;
		if (item == null)
			return;
		ItemStack ITEM = getOption(p, "ITEM").getItemStack();
		if (!ItemUtils.isEquals(item, ITEM))
			return;
		event.setCancelled(true);
		if (GameMain.getPlugin().getCooldownManager().hasCooldown(p.getUniqueId(), getName())) {
			p.playSound(p.getLocation(), Sound.IRONGOLEM_HIT, 0.5F, 1.0F);
			p.sendMessage(GameMain.getPlugin().getCooldownManager().getCooldownFormated(p.getUniqueId(), getName()));
			return;
		}
		Location loc = p.getEyeLocation();
		BlockIterator bta = new BlockIterator(loc, 0.0D, 50);
		while (bta.hasNext()) {
			Location bt = bta.next().getLocation();
			p.getWorld().playEffect(bt, Effect.STEP_SOUND, Material.COAL_BLOCK, 15);
			p.playSound(bt, Sound.GHAST_FIREBALL, 3.0F, 3.0F);
		}
		Snowball s = p.launchProjectile(Snowball.class);
		Vector v = p.getLocation().getDirection().normalize().multiply(10);
		s.setVelocity(v);
		s.setMetadata("blindnessBeam", new FixedMetadataValue(GameMain.getPlugin(), true));
		p.setVelocity(p.getEyeLocation().getDirection().multiply(-0.6F));
		p.sendMessage("§%beam-shot-success%§");
		GameMain.getPlugin().getCooldownManager().setCooldown(p.getUniqueId(), getName(), getOption(p, "COOLDOWN").getValue());
	}

	@EventHandler
	public void onDamageLaser(EntityDamageByEntityEvent e) {
		if (e.getEntity() instanceof Player && e.getDamager() instanceof Snowball) {
			Snowball s = (Snowball) e.getDamager();
			if (s.getShooter() instanceof Player) {
				Player p = (Player) s.getShooter();
				if (s.hasMetadata("blindnessBeam")) {
					Player damaged = (Player) e.getEntity();
					damaged.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, getOption(p, "DURATION").getValue() * 20, 0));
					e.setDamage(getOption(p, "DAMAGE").getValue());
				}
			}
		}
	}

	@Override
	public int getPowerPoints(HashMap<String, CustomOption> map) {
		return (60 - getOption("COOLDOWN", map).getValue()) + (getOption("DURATION", map).getValue() * 2 + getOption("DAMAGE", map).getValue() * 2);
	}
}
