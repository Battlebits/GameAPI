package br.com.battlebits.game.games.hungergames.abilitie;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import br.com.battlebits.game.GameMain;
import br.com.battlebits.game.ability.AbilityRarity;
import br.com.battlebits.game.constructor.Ability;
import br.com.battlebits.game.constructor.CustomOption;
import br.com.battlebits.game.games.hungergames.util.ItemUtils;
import br.com.battlebits.game.interfaces.Disableable;
import net.md_5.bungee.api.ChatColor;

public class KangarooAbility extends Ability implements Disableable {

	private ArrayList<Player> kangaroodj;

	public KangarooAbility() {
		super(new ItemStack(Material.FIREWORK), AbilityRarity.MYSTIC);
		kangaroodj = new ArrayList<>();
		options.put("VECTOR_MULTIPLY", new CustomOption("VECTOR_MULTIPLY", new ItemStack(Material.NETHER_STAR), 1, 8, 10, 12));
		options.put("COOLDOWN", new CustomOption("COOLDOWN", new ItemStack(Material.WATCH), -1, 0, 5, 10));
		options.put("ITEM", new CustomOption("ITEM", new ItemStack(Material.FIREWORK), ChatColor.GOLD + "Kangaroo Boost"));
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onInteract(PlayerInteractEvent event) {
		final Player p = event.getPlayer();
		Action a = event.getAction();
		ItemStack item = p.getItemInHand();
		if (!a.name().contains("RIGHT") && !a.name().contains("LEFT"))
			return;
		if (!hasAbility(p))
			return;
		if (item == null)
			return;
		ItemStack KANGAROO_ITEM = getOption(p, "ITEM").getItemStack();
		if (!ItemUtils.isEquals(item, KANGAROO_ITEM))
			return;
		if (a.name().contains("RIGHT")) {
			event.setCancelled(true);
		}
		item.setDurability(KANGAROO_ITEM.getDurability());
		p.updateInventory();
		if (GameMain.getPlugin().getCooldownManager().hasCooldown(p.getUniqueId(), getName())) {
			p.playSound(p.getLocation(), Sound.IRONGOLEM_HIT, 0.5F, 1.0F);
			p.sendMessage(GameMain.getPlugin().getCooldownManager().getCooldownFormated(p.getUniqueId(), getName()));
			return;
		}
		CustomOption VECTOR_MULTIPLY = getOption(p, "VECTOR_MULTIPLY");
		if (p.isOnGround()) {
			if (!p.isSneaking()) {
				Vector vector = p.getEyeLocation().getDirection();
				vector.multiply(0.3F * (((double) VECTOR_MULTIPLY.getValue()) / 10));
				vector.setY(0.9F * (((double) VECTOR_MULTIPLY.getValue()) / 10));
				p.setVelocity(vector);
				if (kangaroodj.contains(p)) {
					kangaroodj.remove(p);
				}
			} else {
				Vector vector = p.getEyeLocation().getDirection();
				vector.multiply(0.3F * (((double) VECTOR_MULTIPLY.getValue()) / 10));
				vector.setY(0.55F * (((double) VECTOR_MULTIPLY.getValue()) / 10));
				p.setVelocity(vector);
				if (kangaroodj.contains(p)) {
					kangaroodj.remove(p);
				}
			}
		} else {
			if (!kangaroodj.contains(p)) {
				if (!p.isSneaking()) {
					Vector vector = p.getEyeLocation().getDirection();
					vector.multiply(0.3F * (((double) VECTOR_MULTIPLY.getValue()) / 10));
					vector.setY(0.85F * (((double) VECTOR_MULTIPLY.getValue()) / 10));
					p.setVelocity(vector);
					kangaroodj.add(p);
				} else {
					Vector vector = p.getEyeLocation().getDirection();
					vector.multiply(1.3F * (((double) VECTOR_MULTIPLY.getValue()) / 10));
					vector.setY(0.55F * (((double) VECTOR_MULTIPLY.getValue()) / 10));
					p.setVelocity(vector);
					kangaroodj.add(p);
				}
			}
		}
	}

	@EventHandler
	public void onQuit(PlayerQuitEvent event) {
		if (kangaroodj.contains(event.getPlayer()))
			kangaroodj.remove(event.getPlayer());
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onMove(PlayerMoveEvent event) {
		Player p = event.getPlayer();
		if (!hasAbility(p))
			return;
		if (!kangaroodj.contains(p))
			return;
		if (!p.isOnGround())
			return;
		kangaroodj.remove(p);
	}

	@EventHandler
	public void onEntityDamage(EntityDamageByEntityEvent event) {
		if (!(event.getDamager() instanceof Player))
			return;
		if (!(event.getEntity() instanceof Player))
			return;
		Player kangaroo = (Player) event.getEntity();
		if (!hasAbility(kangaroo))
			return;
		if (getOption(kangaroo, "COOLDOWN").getValue() <= 0)
			return;
		GameMain.getPlugin().getCooldownManager().setCooldown(kangaroo.getUniqueId(), getName(), getOption(kangaroo, "COOLDOWN").getValue());
	}

	@Override
	public int getPowerPoints(HashMap<String, CustomOption> map) {
		return (3 * getOption("VECTOR_MULTIPLY", map).getValue()) + (40 - (4 * (getOption("COOLDOWN", map).getValue())));
	}

}
