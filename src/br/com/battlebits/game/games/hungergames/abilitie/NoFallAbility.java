package br.com.battlebits.game.games.hungergames.abilitie;

import java.util.HashMap;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.inventory.ItemStack;

import br.com.battlebits.game.ability.AbilityRarity;
import br.com.battlebits.game.constructor.Ability;
import br.com.battlebits.game.constructor.CustomOption;
import br.com.battlebits.game.interfaces.Disableable;

public class NoFallAbility extends Ability implements Disableable {

	public NoFallAbility() {
		super(new ItemStack(Material.IRON_BOOTS), AbilityRarity.COMMON);
		options.put("MAX_DAMAGE", new CustomOption("MAX_DAMAGE", new ItemStack(Material.REDSTONE), -1, 0, 4, 8));
	}

	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGH)
	public void onDamage(EntityDamageEvent event) {
		if (!(event.getEntity() instanceof Player))
			return;
		if (event.getCause() != DamageCause.FALL)
			return;
		Player p = (Player) event.getEntity();
		CustomOption MAX_DAMAGE = getOption(p, "MAX_DAMAGE");
		if (event.getDamage() < MAX_DAMAGE.getValue())
			return;
		if (hasAbility(p)) {
			event.setCancelled(true);
			p.damage(MAX_DAMAGE.getValue());
		}
	}

	@Override
	public int getPowerPoints(HashMap<String, CustomOption> map) {
		return 50 + 5 * (4 - getOption("MAX_DAMAGE", map).getValue());
	}
}
