
package br.com.battlebits.game.games.hungergames.abilitie;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.inventory.ItemStack;

import br.com.battlebits.game.GameMain;
import br.com.battlebits.game.ability.AbilityRarity;
import br.com.battlebits.game.constructor.Ability;
import br.com.battlebits.game.constructor.CustomOption;
import br.com.battlebits.game.constructor.Gamer;
import br.com.battlebits.game.interfaces.Disableable;

public class StompAbility extends Ability implements Disableable {

	public StompAbility() {
		super(new ItemStack(Material.ANVIL), AbilityRarity.MYSTIC);
		options.put("RADIUS", new CustomOption("RADIUS", new ItemStack(Material.BEACON), 1, 2, 5, 8));
		options.put("POWER", new CustomOption("POWER", new ItemStack(Material.BLAZE_POWDER), 1, 8, 10, 12));
	}

	@EventHandler
	public void onStomper(EntityDamageEvent event) {
		Entity entityStomper = event.getEntity();
		if (!(entityStomper instanceof Player))
			return;
		Player stomper = (Player) entityStomper;
		Gamer gamer = GameMain.getPlugin().getGamerManager().getGamer(stomper.getUniqueId());
		if (!hasAbility(stomper))
			return;
		if (gamer.isGamemaker() || gamer.isSpectator())
			return;
		DamageCause cause = event.getCause();
		if (cause != DamageCause.FALL)
			return;
		double dmg = event.getDamage();
		CustomOption RADIUS = getOption(stomper, "RADIUS");
		CustomOption POWER = getOption(stomper, "POWER");
		boolean hasPlayer = false;
		for (Player stompado : Bukkit.getOnlinePlayers()) {
			if (stompado.getUniqueId() == stomper.getUniqueId())
				continue;
			if (stompado.getLocation().distance(stomper.getLocation()) > RADIUS.getValue()) {
				continue;
			}
			double dmg2 = dmg * (POWER.getValue() / 10d);
			if (stompado.isSneaking() && dmg2 > 8)
				dmg2 = 8;
			stompado.damage(dmg2, stomper);
			hasPlayer = true;
		}
		if (hasPlayer) {
			stomper.getWorld().playSound(stomper.getLocation(), Sound.ANVIL_LAND, 1, 1);
		}
	}

	@Override
	public int getPowerPoints(HashMap<String, CustomOption> map) {
		return getOption("RADIUS", map).getValue() * 4 + getOption("POWER", map).getValue() * 4;
	}

}
