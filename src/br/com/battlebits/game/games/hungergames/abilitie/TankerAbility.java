package br.com.battlebits.game.games.hungergames.abilitie;

import java.util.HashMap;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;

import br.com.battlebits.game.ability.AbilityRarity;
import br.com.battlebits.game.constructor.Ability;
import br.com.battlebits.game.constructor.CustomOption;
import br.com.battlebits.game.interfaces.Disableable;

public class TankerAbility extends Ability implements Disableable {

	public TankerAbility() {
		super(new ItemStack(Material.IRON_CHESTPLATE), AbilityRarity.RARE);
		options.put("REDUCE", new CustomOption("REDUCE", new ItemStack(Material.REDSTONE), 1, 1, 1, 2));
	}

	@EventHandler
	public void onSnail(EntityDamageByEntityEvent event) {
		if (!(event.getEntity() instanceof Player))
			return;
		Player damaged = (Player) event.getEntity();
		if (!hasAbility(damaged))
			return;
		CustomOption REDUCE = getOption(damaged, "REDUCE");
		if (event.getDamage() - REDUCE.getValue() >= 1)
			event.setDamage(event.getDamage() - REDUCE.getValue());
		else
			event.setDamage(1);
	}

	@Override
	public int getPowerPoints(HashMap<String, CustomOption> map) {
		return getOption("REDUCE", map).getValue() * 15;
	}
}
