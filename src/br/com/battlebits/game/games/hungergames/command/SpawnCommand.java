package br.com.battlebits.game.games.hungergames.command;

import br.com.battlebits.commons.bukkit.command.BukkitCommandArgs;
import br.com.battlebits.commons.core.command.CommandClass;
import br.com.battlebits.commons.core.command.CommandFramework.Command;
import br.com.battlebits.game.GameMain;
import br.com.battlebits.game.constructor.Gamer;
import br.com.battlebits.game.games.hungergames.HungerGamesMode;
import br.com.battlebits.game.stage.GameStage;

public class SpawnCommand implements CommandClass {

	@Command(name = "spawn")
	public void abilityList(BukkitCommandArgs args) {
		if (!args.isPlayer())
			return;
		GameStage stage = GameMain.getPlugin().getGameStage();
		Gamer gamer = Gamer.getGamer(args.getPlayer());
		if (!GameStage.isPregame(stage)) {
			if (!gamer.isGamemaker() && !gamer.isSpectator()) {
				args.getPlayer().sendMessage("§%command-spawn-no-access%§");
				return;
			}
		}
		args.getPlayer().sendMessage("§%command-spawn-teleported%§");
		HungerGamesMode.teleportToSpawn(args.getPlayer());
	}

}
