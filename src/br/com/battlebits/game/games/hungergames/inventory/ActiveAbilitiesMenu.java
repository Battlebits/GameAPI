package br.com.battlebits.game.games.hungergames.inventory;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.com.battlebits.commons.core.translate.T;
import br.com.battlebits.game.GameMain;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import br.com.battlebits.commons.api.item.ItemBuilder;
import br.com.battlebits.commons.api.menu.ClickType;
import br.com.battlebits.commons.api.menu.MenuClickHandler;
import br.com.battlebits.commons.api.menu.MenuInventory;
import br.com.battlebits.commons.api.menu.MenuItem;
import br.com.battlebits.commons.core.account.BattlePlayer;
import br.com.battlebits.commons.core.translate.Translate;
import br.com.battlebits.game.constructor.Ability;
import br.com.battlebits.game.constructor.CustomKit;
import br.com.battlebits.game.games.hungergames.inventory.finder.AbilityFinder;
import br.com.battlebits.game.util.NameUtils;
import net.md_5.bungee.api.ChatColor;

public class ActiveAbilitiesMenu {

	public static void open(Player player, CustomKit customKit, AbilityFinder finder, MenuInventory topInventory) {
		MenuInventory menu = new MenuInventory(NameUtils.formatString(customKit.getName()) + " ["
				+ (customKit.getPowerPoints() > 100 ? ChatColor.RED + "" : ChatColor.GREEN) + customKit.getPowerPoints()
				+ ChatColor.RESET + "/100]", 6, true);
		List<Ability> abilityList = customKit.getAbilities();
		Collections.sort(abilityList, new Comparator<Ability>() {

			@Override
			public int compare(Ability o1, Ability o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});
		ItemStack nullItem = new ItemBuilder().type(Material.STAINED_GLASS_PANE).durability(15).name(" ").build();
		menu.setItem(0,
				new MenuItem(new ItemBuilder().type(Material.BED).name("§%back%§").build(), new MenuClickHandler() {
					@Override
					public void onClick(Player arg0, Inventory arg1, ClickType arg2, ItemStack arg3, int arg4) {
						if (topInventory != null)
							topInventory.open(arg0);
						else
							arg0.closeInventory();
					}
				}));
		int w = 19;
		for (int i = 0; i < abilityList.size(); i++) {
			Ability ability = abilityList.get(i);
			ItemStack item = new ItemBuilder().type(ability.getIcon().getType())
					.durability(ability.getIcon().getDurability())//
					.name(ability.getRarity().getColor() + T.t(GameMain.getPlugin(), BattlePlayer.getLanguage(player.getUniqueId()), "ability-inventory-name")
							.replace("%ability%", NameUtils.formatString(ability.getName())))//
					.lore(Arrays.asList("§%" + ability.getRarity().toString().toLowerCase() + "%§", "",
							"§%ability-inventory-click%§"))
					.build();
			MenuClickHandler itemHandler = new MenuClickHandler() {
				@Override
				public void onClick(Player arg0, Inventory arg1, ClickType arg2, ItemStack arg3, int arg4) {
					finder.onAbilityFound(ability);
				}
			};
			menu.setItem(w, new MenuItem(item, itemHandler));
			if (w % 9 == 7) {
				w += 3;
				continue;
			}
			w += 1;
		}
		if (abilityList.size() == 0) {
			menu.setItem(31, new ItemBuilder().type(Material.PAINTING).name("§c§lOps!")
					.lore(Arrays.asList("§%kit-dont-have-abilities%§")).build());
		}

		for (int i = 0; i < 9; i++) {
			if (menu.getItem(i) == null)
				menu.setItem(i, nullItem);
		}
		menu.open(player);
	}

}
