package br.com.battlebits.game.games.hungergames.inventory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.com.battlebits.commons.core.translate.T;
import br.com.battlebits.game.GameMain;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import br.com.battlebits.commons.api.input.InputAPI;
import br.com.battlebits.commons.api.input.InputHandler;
import br.com.battlebits.commons.api.item.ItemBuilder;
import br.com.battlebits.commons.api.menu.ClickType;
import br.com.battlebits.commons.api.menu.MenuClickHandler;
import br.com.battlebits.commons.api.menu.MenuInventory;
import br.com.battlebits.commons.api.menu.MenuItem;
import br.com.battlebits.commons.core.account.BattlePlayer;
import br.com.battlebits.commons.core.translate.Translate;
import br.com.battlebits.game.constructor.Ability;
import br.com.battlebits.game.constructor.Gamer;
import br.com.battlebits.game.games.hungergames.inventory.finder.AbilityFinder;
import br.com.battlebits.game.manager.AbilityManager;
import br.com.battlebits.game.util.NameUtils;
import br.com.battlebits.game.util.SearchUtils;

public class AddAbilityMenu {
	private static int itemsPerPage = 21;

	public static void open(Player player, MenuInventory topInventory, AbilityFinder finder, int page, String search) {
		List<Ability> abilityList = new ArrayList<>(AbilityManager.getAbilities().values());
		if (search != null && !search.isEmpty())
			abilityList = SearchUtils.searchAbilities(search, abilityList);
		if (abilityList.size() == 1) {
			finder.onAbilityFound(abilityList.get(0));
		}
		Collections.sort(abilityList, new Comparator<Ability>() {

			@Override
			public int compare(Ability o1, Ability o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});
		MenuInventory menu = new MenuInventory(
				"§%ability_Finder%§ [" + page + "/" + ((int) Math.ceil(abilityList.size() / itemsPerPage) + 1) + "]", 6,
				true);
		ItemStack nullItem = new ItemBuilder().type(Material.STAINED_GLASS_PANE).durability(15).name(" ").build();
		Gamer gamer = Gamer.getGamer(player);
		menu.setItem(0,
				new MenuItem(new ItemBuilder().type(Material.BED).name("§%back%§").build(), new MenuClickHandler() {
					@Override
					public void onClick(Player arg0, Inventory arg1, ClickType arg2, ItemStack arg3, int arg4) {
						if (topInventory != null)
							topInventory.open(arg0);
						else
							arg0.closeInventory();
					}
				}));
		menu.setItem(6, new MenuItem(new ItemBuilder().type(Material.COMPASS).name("§%search%§").build(),
				new MenuClickHandler() {
					@Override
					public void onClick(Player arg0, Inventory arg1, ClickType arg2, ItemStack arg3, int arg4) {

						InputAPI.openAnvilGui(arg0, "", new ItemBuilder().type(Material.COMPASS).name("§%search%§")
								.lore("§%search-for-ability%§").build(), new InputHandler() {

									@Override
									public void onDone(Player p, String name) {
										if (name.isEmpty()) {
											menu.open(p);
										} else {
											try {
												menu.close(p);
												new AddAbilityMenu(player, topInventory, finder, 1, name);
											} catch (Exception e) {
												menu.close(p);
												new AddAbilityMenu(player, topInventory, finder, 1, name);
											}
										}
									}

									@Override
									public void onClose(Player p) {
										menu.open(p);
									}
								});
					}
				}));

		int pageStart = 0;
		int pageEnd = itemsPerPage;
		if (page > 1) {
			pageStart = ((page - 1) * itemsPerPage);
			pageEnd = (page * itemsPerPage);
		}
		if (pageEnd > abilityList.size()) {
			pageEnd = abilityList.size();
		}
		if (page == 1) {
			menu.setItem(27,
					new ItemBuilder().type(Material.INK_SACK).durability(8).name("§%page-last-dont-have%§").build());
		} else {
			menu.setItem(27,
					new MenuItem(new ItemBuilder().type(Material.INK_SACK).durability(10).name("§%page-last-page%§")
							.lore(Arrays.asList("§%page-last-click-here%§")).build(), new MenuClickHandler() {
								@Override
								public void onClick(Player arg0, Inventory arg1, ClickType arg2, ItemStack arg3,
										int arg4) {
									new AddAbilityMenu(arg0, topInventory, finder, page - 1, null);
								}
							}));
		}

		if (Math.ceil(abilityList.size() / itemsPerPage) + 1 > page) {
			menu.setItem(35,
					new MenuItem(new ItemBuilder().type(Material.INK_SACK).durability(10).name("§%page-next-page%§")
							.lore(Arrays.asList("§%page-next-click-here%§")).build(), new MenuClickHandler() {
								@Override
								public void onClick(Player arg0, Inventory arg1, ClickType arg2, ItemStack arg3,
										int arg4) {
									new AddAbilityMenu(arg0, topInventory, finder, page + 1, null);
								}
							}));
		} else {
			menu.setItem(35,
					new ItemBuilder().type(Material.INK_SACK).durability(8).name("§%page-next-dont-have%§").build());
		}
		int w = 19;
		MenuClickHandler dontHaveKit = new MenuClickHandler() {
			@Override
			public void onClick(Player arg0, Inventory arg1, ClickType arg2, ItemStack arg3, int arg4) {
				// TODO Mandar mensagem
				arg0.sendMessage("Para fazer: Voc§ n§o possui a habilidade");
			}
		};
		for (int i = pageStart; i < pageEnd; i++) {
			Ability ability = abilityList.get(i);
			if (gamer.hasAbility(ability)) {
				ItemStack item = new ItemBuilder().type(ability.getIcon().getType())
						.durability(ability.getIcon().getDurability())//
						.name(ability.getRarity().getColor() + T.t(GameMain.getPlugin(), BattlePlayer.getLanguage(player.getUniqueId()),
										"ability-inventory-name")
								.replace("%ability%", NameUtils.formatString(ability.getName())))//
						.lore(Arrays.asList("§%" + ability.getRarity().toString().toLowerCase() + "%§", "",
								"§%ability-inventory-click%§"))
						.build();
				MenuClickHandler itemHandler = new MenuClickHandler() {
					@Override
					public void onClick(Player arg0, Inventory arg1, ClickType arg2, ItemStack arg3, int arg4) {
						finder.onAbilityFound(ability);
					}
				};
				menu.setItem(w, new MenuItem(item, itemHandler));
			} else {
				ItemStack item = new ItemBuilder().type(Material.STAINED_GLASS_PANE).durability(14)//
						.name(T.t(GameMain.getPlugin(), BattlePlayer.getLanguage(player.getUniqueId()),
										"ability-inventory-dont-have")
								.replace("%ability%", NameUtils.formatString(ability.getName())))//
						.lore(Arrays.asList("§%ability-inventory-click-buy%§")).build();
				menu.setItem(w, new MenuItem(item, dontHaveKit));
			}
			if (w % 9 == 7) {
				w += 3;
				continue;
			}
			w += 1;
		}
		if (abilityList.size() == 0) {
			menu.setItem(31, new ItemBuilder().type(Material.PAINTING).name("§c§lOps!")
					.lore(Arrays.asList("§%nothing-found%§")).build());
		}

		for (int i = 0; i < 9; i++) {
			if (menu.getItem(i) == null)
				menu.setItem(i, nullItem);
		}
		menu.open(player);
	}

}
