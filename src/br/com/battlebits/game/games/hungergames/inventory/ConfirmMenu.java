package br.com.battlebits.game.games.hungergames.inventory;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import br.com.battlebits.commons.api.item.ItemBuilder;
import br.com.battlebits.commons.api.menu.ClickType;
import br.com.battlebits.commons.api.menu.MenuClickHandler;
import br.com.battlebits.commons.api.menu.MenuInventory;
import br.com.battlebits.commons.api.menu.MenuItem;

public class ConfirmMenu {

	public static void open(Player player, ConfirmHandler handler, MenuInventory topInventory) {
		MenuInventory menu = new MenuInventory("§%remove-confirm%§", 6, true);
		ItemStack nullItem = new ItemBuilder().type(Material.STAINED_GLASS_PANE).durability(15).name(" ").build();
		menu.setItem(0, new MenuItem(new ItemBuilder().type(Material.BED).name("§%back%§").build(), new MenuClickHandler() {
			@Override
			public void onClick(Player arg0, Inventory arg1, ClickType arg2, ItemStack arg3, int arg4) {
				if (topInventory != null)
					topInventory.open(arg0);
				else
					arg0.closeInventory();
			}
		}));

		MenuClickHandler yesHandler = new MenuClickHandler() {
			@Override
			public void onClick(Player arg0, Inventory arg1, ClickType arg2, ItemStack arg3, int arg4) {
				handler.onCofirm(true);
			}
		};

		MenuClickHandler noHandler = new MenuClickHandler() {
			@Override
			public void onClick(Player arg0, Inventory arg1, ClickType arg2, ItemStack arg3, int arg4) {
				handler.onCofirm(false);
			}
		};

		menu.setItem(20, new MenuItem(new ItemBuilder().type(Material.REDSTONE_BLOCK).name("§%deny%§").build(), noHandler));
		menu.setItem(21, new MenuItem(new ItemBuilder().type(Material.REDSTONE_BLOCK).name("§%deny%§").build(), noHandler));
		menu.setItem(29, new MenuItem(new ItemBuilder().type(Material.REDSTONE_BLOCK).name("§%deny%§").build(), noHandler));
		menu.setItem(30, new MenuItem(new ItemBuilder().type(Material.REDSTONE_BLOCK).name("§%deny%§").build(), noHandler));

		menu.setItem(23, new MenuItem(new ItemBuilder().type(Material.EMERALD_BLOCK).name("§%confirm%§").build(), yesHandler));
		menu.setItem(24, new MenuItem(new ItemBuilder().type(Material.EMERALD_BLOCK).name("§%confirm%§").build(), yesHandler));
		menu.setItem(32, new MenuItem(new ItemBuilder().type(Material.EMERALD_BLOCK).name("§%confirm%§").build(), yesHandler));
		menu.setItem(33, new MenuItem(new ItemBuilder().type(Material.EMERALD_BLOCK).name("§%confirm%§").build(), yesHandler));

		for (int i = 0; i < 9; i++) {
			if (menu.getItem(i) == null)
				menu.setItem(i, nullItem);
		}

		for (int i = 45; i < 54; i++) {
			if (menu.getItem(i) == null)
				menu.setItem(i, nullItem);
		}
		menu.open(player);
	}

	public static interface ConfirmHandler {
		public void onCofirm(boolean confirmed);
	}
}
