package br.com.battlebits.game.games.hungergames.inventory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import br.com.battlebits.commons.api.input.InputAPI;
import br.com.battlebits.commons.api.input.InputHandler;
import br.com.battlebits.commons.api.item.ItemBuilder;
import br.com.battlebits.commons.api.menu.ClickType;
import br.com.battlebits.commons.api.menu.MenuClickHandler;
import br.com.battlebits.commons.api.menu.MenuInventory;
import br.com.battlebits.commons.api.menu.MenuItem;
import br.com.battlebits.game.GameMain;
import br.com.battlebits.game.constructor.Ability;
import br.com.battlebits.game.constructor.CustomKit;
import br.com.battlebits.game.constructor.Gamer;
import br.com.battlebits.game.constructor.Gamer.CustomKitType;
import br.com.battlebits.game.data.DataGamer;
import br.com.battlebits.game.games.hungergames.inventory.ConfirmMenu.ConfirmHandler;
import br.com.battlebits.game.games.hungergames.inventory.finder.AbilityFinder;
import br.com.battlebits.game.games.hungergames.inventory.finder.EditHandler;
import br.com.battlebits.game.games.hungergames.inventory.finder.ItemFinder;
import br.com.battlebits.game.util.NameUtils;

public class CustomKitMenu {

	private CustomKit customKit;
	private Player player;
	private MenuInventory menu;

	public CustomKitMenu(Player player, CustomKit kit, CustomKitType type, boolean selector) {
		this.customKit = kit.clone();
		this.player = player;
		menu = new MenuInventory(NameUtils.formatString(customKit.getName()) + " " + getPowerPoint(), 6, true);

		menu.setItem(0,
				new MenuItem(new ItemBuilder().type(Material.BED).name("§%back%§").build(), new MenuClickHandler() {
					@Override
					public void onClick(Player arg0, Inventory arg1, ClickType arg2, ItemStack arg3, int arg4) {
						if (selector)
							CustomKitSelectorMenu.open(arg0, 1);
						else
							arg0.closeInventory();
					}
				}));

		menu.setItem(21,
				new MenuItem(new ItemBuilder().type(Material.BOOK_AND_QUILL).name("§%view-abilities%§").build(),
						new MenuClickHandler() {
							@Override
							public void onClick(Player arg0, Inventory arg1, ClickType arg2, ItemStack arg3, int arg4) {
								ActiveAbilitiesMenu.open(arg0, customKit, new AbilityFinder() {
									@Override
									public void onAbilityFound(Ability ability) {
										new EditAbilityMenu(arg0, new EditHandler() {

											@Override
											public void onFinish() {
												updateTitle();
												updateAddAbility();
												updateKitItems();
												menu.open(player);
											}
										}, customKit, ability, menu);
									}
								}, menu);
							}
						}));

		update();
		if (Gamer.getGamer(player).hasCustomKitName(kit.getName())) {

			menu.setItem(8, new MenuItem(new ItemBuilder().type(Material.REDSTONE_BLOCK).name("§%remove-kit%§").build(),
					new MenuClickHandler() {
						@Override
						public void onClick(Player arg0, Inventory arg1, ClickType arg2, ItemStack arg3, int arg4) {
							ConfirmMenu.open(player, new ConfirmHandler() {
								@Override
								public void onCofirm(boolean confirmed) {
									if (confirmed) {
										Gamer gamer = Gamer.getGamer(arg0);
										gamer.getCustomKits().get(type).remove(kit.getName());
										DataGamer.saveGamerField(gamer, "customKits");
										GameMain.getPlugin().getKitManager().unregisterPlayer(player);
										CustomKitSelectorMenu.open(arg0, 1);
									} else {
										menu.open(arg0);
									}
								}
							}, menu);
						}
					}));
		}
		ItemStack save = new ItemBuilder().type(Material.EMERALD_BLOCK).name("§%save%§").build();
		MenuClickHandler handler = new MenuClickHandler() {
			@Override
			public void onClick(Player arg0, Inventory arg1, ClickType arg2, ItemStack arg3, int arg4) {
				if (!Gamer.getGamer(arg0).getCustomKits().containsKey(type))
					Gamer.getGamer(arg0).getCustomKits().put(type, new HashMap<>());
				Gamer gamer = Gamer.getGamer(arg0);
				gamer.getCustomKits().get(type).remove(kit.getName());
				gamer.getCustomKits().get(type).put(customKit.getName(), customKit);
				DataGamer.saveGamerField(gamer, "customKits");
				GameMain.getPlugin().getKitManager().selectKit(arg0, customKit);
				if (selector)
					CustomKitSelectorMenu.open(arg0, 1);
				else
					arg0.closeInventory();
			}
		};
		menu.setItem(43, new MenuItem(save, handler));
		menu.setItem(44, new MenuItem(save, handler));
		menu.setItem(52, new MenuItem(save, handler));
		menu.setItem(53, new MenuItem(save, handler));

		ItemStack nullItem = new ItemBuilder().type(Material.STAINED_GLASS_PANE).durability(15).name(" ").build();

		for (int i = 0; i < 9; i++) {
			if (menu.getItem(i) == null)
				menu.setItem(i, nullItem);
		}
		for (int i = 45; i < 54; i++) {
			if (menu.getItem(i) == null)
				menu.setItem(i, nullItem);
		}
		menu.open(player);
	}

	public void update() {
		updateAddAbility();
		updateName();
		updateIcon();
		updateKitItems();
	}

	public void updateTitle() {
		menu.setTitle(NameUtils.formatString(customKit.getName()) + " " + getPowerPoint());
	}

	public void updateAddAbility() {
		ItemBuilder builder = new ItemBuilder().type(Material.WORKBENCH).name("§%add-abilities%§");
		MenuClickHandler handler = new MenuClickHandler() {
			@Override
			public void onClick(Player arg0, Inventory arg1, ClickType arg2, ItemStack arg3, int arg4) {

			}
		};
		if (customKit.getPowerPoints() >= 100) {
			builder = builder.lore(Arrays.asList("§%powerPoint-limit%§"));
		} else {
			handler = new MenuClickHandler() {
				@Override
				public void onClick(Player arg0, Inventory arg1, ClickType arg2, ItemStack arg3, int arg4) {
					AbilityFinder finder = new AbilityFinder() {
						@Override
						public void onAbilityFound(Ability ability) {
							new EditAbilityMenu(arg0, new EditHandler() {

								@Override
								public void onFinish() {
									updateTitle();
									updateAddAbility();
									updateKitItems();
									menu.open(player);
								}
							}, customKit, ability, menu);
						}
					};
					AddAbilityMenu.open(player, menu, finder, 1, null);
				}
			};
		}
		menu.setItem(19, new MenuItem(builder.build(), handler));
	}

	private String getPowerPoint() {
		return "[" + (customKit.getPowerPoints() > 100 ? ChatColor.RED + "" : ChatColor.GREEN)
				+ customKit.getPowerPoints() + ChatColor.RESET + "/100]";
	}

	public void updateKitItems() {
		menu.setItem(36, new ItemBuilder().type(Material.SIGN).name("§%kit-items%§").build());

		List<ItemStack> kitItems = new ArrayList<>();
		for (Ability ability : customKit.getAbilities()) {
			kitItems.addAll(ability.getItems(customKit));
		}
		for (int i = 0; i < kitItems.size(); i++) {
			menu.setItem(45 + i, kitItems.get(i));
		}
		for (int i = 45 + kitItems.size(); i < 52; i++) {
			menu.setItem(i, new ItemBuilder().type(Material.STAINED_GLASS_PANE).durability(15).name(" ").build());
		}
		menu.setItem(45 + kitItems.size(), new ItemStack(Material.COMPASS));

	}

	public void updateName() {
		menu.setItem(25,
				new MenuItem(new ItemBuilder().type(Material.NAME_TAG).name("§%change_Name%§")
						.lore(Arrays.asList(" ", ChatColor.GRAY + NameUtils.formatString(customKit.getName()))).build(),
						new MenuClickHandler() {
							@Override
							public void onClick(Player arg0, Inventory arg1, ClickType arg2, ItemStack arg3, int arg4) {
								InputAPI.openAnvilGui(arg0, "", new ItemBuilder().type(Material.PAPER).name("§%type%§")
										.lore("§%type-kit-name%§").build(), new InputHandler() {

											@Override
											public void onDone(Player p, String name) {
												if (!name.isEmpty()) {
													name = name.toLowerCase().replaceAll("\\s+", "");
													name = name.substring(0, name.length() > 18 ? 18 : name.length());
													if (name.length() < 3) {
														p.sendMessage("§%kitname-more-than-3-char%§");
														return;
													}
													if (Gamer.getGamer(p).hasCustomKitName(name)) {
														p.sendMessage("§%player-already-have-customkit-name%§");
														return;
													}
													customKit.setName(name);
													updateTitle();
													updateName();
												}
												menu.open(p);
											}

											@Override
											public void onClose(Player p) {
												menu.open(p);
											}
										});
							}
						}));
	}

	public void updateIcon() {
		menu.setItem(23,
				new MenuItem(
						new ItemBuilder().type(customKit.getIcon().getType())
								.durability(customKit.getIcon().getDurability()).name("§%change_Icon%§").build(),
						new MenuClickHandler() {
							@Override
							public void onClick(Player arg0, Inventory arg1, ClickType arg2, ItemStack arg3, int arg4) {
								ItemFinderMenu.open(arg0, menu, new ItemFinder() {

									@Override
									public void onSelect(ItemStack item) {
										customKit.setIcon(item);
										updateIcon();
										menu.open(arg0);
									}
								}, 1, null, false);
							}
						}));
	}
}
