package br.com.battlebits.game.games.hungergames.inventory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import br.com.battlebits.commons.api.item.ItemBuilder;
import br.com.battlebits.commons.api.menu.ClickType;
import br.com.battlebits.commons.api.menu.MenuClickHandler;
import br.com.battlebits.commons.api.menu.MenuInventory;
import br.com.battlebits.commons.api.menu.MenuItem;
import br.com.battlebits.commons.core.account.BattlePlayer;
import br.com.battlebits.commons.core.permission.Group;
import br.com.battlebits.game.GameMain;
import br.com.battlebits.game.constructor.CustomKit;
import br.com.battlebits.game.constructor.Gamer;
import br.com.battlebits.game.constructor.Gamer.CustomKitType;
import br.com.battlebits.game.util.NameUtils;

public class CustomKitSelectorMenu {
	private static int itemsPerPage = 21;

	public static void open(Player player, int page) {
		Gamer gamer = Gamer.getGamer(player);
		BattlePlayer battlePlayer = BattlePlayer.getPlayer(player.getUniqueId());

		List<CustomKit> customKits = new ArrayList<>();
		List<CustomKit> yourKits = new ArrayList<>(gamer.getYourCustomKits().values());
		List<CustomKit> vipKits = new ArrayList<>(gamer.getVipCustomKits().values());
		List<CustomKit> fullKits = new ArrayList<>(gamer.getFullCustomKits().values());
		Comparator<CustomKit> comparator = new Comparator<CustomKit>() {
			@Override
			public int compare(CustomKit o1, CustomKit o2) {
				return o1.getName().compareTo(o2.getName());
			}
		};

		Collections.sort(yourKits, comparator);
		Collections.sort(vipKits, comparator);
		Collections.sort(fullKits, comparator);

		customKits.addAll(yourKits);
		customKits.addAll(vipKits);
		customKits.addAll(fullKits);

		Iterator<CustomKit> k = yourKits.iterator();

		while (yourKits.size() > gamer.getCustomKitSlots()) {
			CustomKit cus = k.next();
			gamer.getYourCustomKits().remove(cus.getName());
			k.remove();
		}

		MenuInventory menu = new MenuInventory("§%your_Custom_Kits%§ [" + page + "/" + ((int) Math.ceil(customKits.size() / itemsPerPage) + 1) + "]", 6, true);

		ItemStack nullItem = new ItemBuilder().type(Material.STAINED_GLASS_PANE).durability(15).name(" ").build();

		List<MenuItem> items = new ArrayList<>();
		for (int i = 0; i < yourKits.size(); i++) {
			CustomKit kit = yourKits.get(i);
			items.add(new MenuItem(new ItemBuilder().lore("§%right-click-to-edit%§").type(kit.getIcon().getType()).durability(kit.getIcon().getDurability()).name(ChatColor.RESET + "Kit " + NameUtils.formatString(kit.getName())).build(), new OpenCustomKitMenu(kit, CustomKitType.NORMAL, true)));
		}

		String kitName = "CustomKit#";
		int a = 1;
		while (gamer.hasCustomKitName(kitName = "CustomKit#" + a))
			a++;

		for (int i = 0; i < gamer.getCustomKitSlots() - yourKits.size(); i++) {
			items.add(new MenuItem(new ItemBuilder().type(Material.STAINED_GLASS_PANE).durability(5).name("§%slot-disponivel%§").build(), new OpenCustomKitMenu(new CustomKit(kitName, new ItemStack(Material.STONE)), CustomKitType.NORMAL, false)));
		}

		for (int i = 0; i < vipKits.size(); i++) {
			CustomKit kit = vipKits.get(i);
			if (i < getVipSlots(battlePlayer.getServerGroup())) {
				items.add(new MenuItem(new ItemBuilder().lore("§%right-click-to-edit%§").type(kit.getIcon().getType()).durability(kit.getIcon().getDurability()).name(ChatColor.LIGHT_PURPLE + "Kit " + NameUtils.formatString(kit.getName())).build(), new OpenCustomKitMenu(kit, CustomKitType.VIP, true)));
			} else {
				items.add(new MenuItem(new ItemBuilder().lore("§%right-click-to-edit%§").type(Material.STAINED_GLASS_PANE).durability(14).name(ChatColor.RED + "Kit " + NameUtils.formatString(kit.getName())).build(), new OpenCustomKitMenu(kit, CustomKitType.VIP, false)));
			}
		}
		if (getVipSlots(battlePlayer.getServerGroup()) - vipKits.size() > 0)
			for (int i = 0; i < getVipSlots(battlePlayer.getServerGroup()) - vipKits.size(); i++) {
				items.add(new MenuItem(new ItemBuilder().type(Material.STAINED_GLASS_PANE).durability(6).name("§%slot-vip-disponivel%§").build(), new OpenCustomKitMenu(new CustomKit(kitName, new ItemStack(Material.STONE)), CustomKitType.VIP, false)));
			}

		for (int i = 0; i < fullKits.size(); i++) {
			CustomKit kit = fullKits.get(i);
			if (battlePlayer.hasGroupPermission(Group.YOUTUBER)) {
				items.add(new MenuItem(new ItemBuilder().lore("§%right-click-to-edit%§").type(kit.getIcon().getType()).durability(kit.getIcon().getDurability()).name(ChatColor.AQUA + "Kit " + NameUtils.formatString(kit.getName())).build(), new OpenCustomKitMenu(kit, CustomKitType.FULL, true)));
			} else {
				items.add(new MenuItem(new ItemBuilder().lore("§%right-click-to-edit%§").type(Material.STAINED_GLASS_PANE).durability(14).name(ChatColor.RED + "Kit " + NameUtils.formatString(kit.getName())).build(), new OpenCustomKitMenu(kit, CustomKitType.FULL, false)));
			}
		}
		if (battlePlayer.hasGroupPermission(Group.YOUTUBER))
			items.add(new MenuItem(new ItemBuilder().type(Material.STAINED_GLASS_PANE).durability(3).name("§%slot-full-disponivel%§").build(), new OpenCustomKitMenu(new CustomKit(kitName, new ItemStack(Material.STONE)), CustomKitType.FULL, false)));

		// PAGINA§§O
		int pageStart = 0;
		int pageEnd = itemsPerPage;
		if (page > 1) {
			pageStart = ((page - 1) * itemsPerPage);
			pageEnd = (page * itemsPerPage);
		}
		if (pageEnd > items.size()) {
			pageEnd = items.size();
		}
		if (page == 1) {
			menu.setItem(new ItemBuilder().type(Material.INK_SACK).durability(8).name("§%page-last-dont-have%§").build(), 27);
		} else {
			menu.setItem(new MenuItem(new ItemBuilder().type(Material.INK_SACK).durability(10).name("§%page-last-page%§").lore(Arrays.asList("§%page-last-click-here%§")).build(), new MenuClickHandler() {
				@Override
				public void onClick(Player arg0, Inventory arg1, ClickType arg2, ItemStack arg3, int arg4) {
					CustomKitSelectorMenu.open(arg0, page - 1);
				}
			}), 27);
		}

		if (Math.ceil(items.size() / itemsPerPage) + 1 > page) {
			menu.setItem(new MenuItem(new ItemBuilder().type(Material.INK_SACK).durability(10).name("§%page-next-page%§").lore(Arrays.asList("§%page-next-click-here%§")).build(), new MenuClickHandler() {
				@Override
				public void onClick(Player arg0, Inventory arg1, ClickType arg2, ItemStack arg3, int arg4) {
					CustomKitSelectorMenu.open(arg0, page + 1);
				}
			}), 35);
		} else {
			menu.setItem(new ItemBuilder().type(Material.INK_SACK).durability(8).name("§%page-next-dont-have%§").build(), 35);
		}

		// CUSTOM KITS

		int w = 19;

		for (int i = pageStart; i < pageEnd; i++) {
			MenuItem item = items.get(i);
			menu.setItem(item, w);
			if (w % 9 == 7) {
				w += 3;
				continue;
			}
			w += 1;
		}
		if (items.size() == 0) {
			menu.setItem(new ItemBuilder().type(Material.PAINTING).name("§c§lOps!").lore(Arrays.asList("§%error%§")).build(), 31);
		}

		for (int i = 0; i < 9; i++) {
			if (menu.getItem(i) == null)
				menu.setItem(nullItem, i);
		}
		menu.open(player);
	}

	private static int getVipSlots(Group group) {
		if (group == Group.LIGHT)
			return 1;
		if (group == Group.PREMIUM)
			return 3;
		if (group == Group.ULTIMATE)
			return 5;
		return 0;
	}

	public static class OpenCustomKitMenu implements MenuClickHandler {

		private CustomKit kit;
		private CustomKitType type;
		private boolean leftSelect;

		public OpenCustomKitMenu(CustomKit kit, CustomKitType type, boolean leftSelect) {
			this.kit = kit;
			this.type = type;
			this.leftSelect = leftSelect;
		}

		@Override
		public void onClick(Player arg0, Inventory arg1, ClickType arg2, ItemStack arg3, int arg4) {
			if (ClickType.LEFT == arg2 && leftSelect) {
				GameMain.getPlugin().getKitManager().selectKit(arg0, kit);
				return;
			}
			new CustomKitMenu(arg0, kit, type, true);
		}

	}

}
