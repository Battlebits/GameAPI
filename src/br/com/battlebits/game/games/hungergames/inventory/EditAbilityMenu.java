package br.com.battlebits.game.games.hungergames.inventory;

import java.util.Arrays;
import java.util.HashMap;

import br.com.battlebits.commons.core.translate.T;
import br.com.battlebits.game.GameMain;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import br.com.battlebits.commons.api.item.ItemBuilder;
import br.com.battlebits.commons.api.menu.ClickType;
import br.com.battlebits.commons.api.menu.MenuClickHandler;
import br.com.battlebits.commons.api.menu.MenuInventory;
import br.com.battlebits.commons.api.menu.MenuItem;
import br.com.battlebits.commons.core.account.BattlePlayer;
import br.com.battlebits.commons.core.translate.Translate;
import br.com.battlebits.game.constructor.Ability;
import br.com.battlebits.game.constructor.CustomKit;
import br.com.battlebits.game.constructor.CustomOption;
import br.com.battlebits.game.games.hungergames.inventory.finder.EditHandler;
import br.com.battlebits.game.games.hungergames.inventory.finder.ItemFinder;
import br.com.battlebits.game.util.NameUtils;
import net.md_5.bungee.api.ChatColor;

public class EditAbilityMenu {
	private Ability ability;
	private CustomKit customKit;
	private Player player;
	private MenuInventory menu;
	private HashMap<String, CustomOption> options = new HashMap<>();
	private EditHandler handler;

	public int getKitPowerPoint() {
		int powerPoint = customKit.getPowerPoints();
		if (customKit.getAbilities().contains(ability))
			powerPoint = customKit.getPowerPoints() - ability.getPowerPoints(options);
		return powerPoint;
	}

	public EditAbilityMenu(Player player, EditHandler handler, CustomKit customKit, Ability ability, MenuInventory topInventory) {
		this.player = player;
		this.handler = handler;
		if (customKit.getAbilities().contains(ability))
			options = customKit.abilityOption(ability.getName());
		this.customKit = customKit;
		this.ability = ability;
		menu = new MenuInventory(NameUtils.formatString(customKit.getName()) + " " + getPowerPoint(), 6, true);
		ItemStack nullItem = new ItemBuilder().type(Material.STAINED_GLASS_PANE).durability(15).name(" ").build();

		menu.setItem(new MenuItem(new ItemBuilder().type(Material.BED).name("§%back%§").build(), new MenuClickHandler() {
			@Override
			public void onClick(Player arg0, Inventory arg1, ClickType arg2, ItemStack arg3, int arg4) {
				if (topInventory != null)
					handler.onFinish();
				else
					arg0.closeInventory();
			}
		}), 0);
		updateEditItem();
		updateItemCustom();
		updateOptions();
		for (int i = 0; i < 9; i++) {
			if (menu.getItem(i) == null)
				menu.setItem(nullItem, i);
		}
		menu.open(player);
	}

	private String getPowerPoint() {
		int pp = getKitPowerPoint() + ability.getPowerPoints(options);
		return "[" + (pp > 100 ? ChatColor.RED + "" : ChatColor.GREEN) + pp + ChatColor.RESET + "/100]";
	}

	public void updateEditItem() {
		if (customKit.getAbilities().contains(ability)) {
			ItemStack removeAbility = new ItemBuilder().type(Material.REDSTONE_BLOCK)//
					.name("§%removeAbility%§")//
					.lore(Arrays.asList(T.t(GameMain.getPlugin(),
									BattlePlayer.getLanguage(player.getUniqueId()), "ability-will-be-pp")//
							.replace("%will%", getKitPowerPoint() + "")))//
					.build();
			menu.setItem(new MenuItem(removeAbility, new MenuClickHandler() {
				@Override
				public void onClick(Player arg0, Inventory arg1, ClickType arg2, ItemStack arg3, int arg4) {
					customKit.removeOption(ability.getName());
					customKit.removeAbility(ability);
					handler.onFinish();
				}
			}), 4);

			ItemStack addAbility = new ItemBuilder().type(Material.DIAMOND)//
					.name("§%editAbility%§")//
					.lore(Arrays.asList(T.t(GameMain.getPlugin(),//
									BattlePlayer.getLanguage(player.getUniqueId()), "ability-will-costs-pp")//
							.replace("%cost%",
									ability//
											.getPowerPoints(options) + "")))//
					.build();
			menu.setItem(new MenuItem(addAbility, new MenuClickHandler() {
				@SuppressWarnings("unchecked")
				@Override
				public void onClick(Player arg0, Inventory arg1, ClickType arg2, ItemStack arg3, int arg4) {
					for (CustomOption option : ((HashMap<String, CustomOption>) options.clone()).values()) {
						customKit.setOption(ability.getName(), option.getName(), option);
					}
					handler.onFinish();
				}
			}), 8);
		} else {
			ItemStack addAbility = new ItemBuilder().type(Material.EMERALD)//
					.name("§%addAbility%§")//
					.lore(Arrays.asList(T.t(GameMain.getPlugin(),//
									BattlePlayer.getLanguage(player.getUniqueId()), "ability-will-costs-pp")//
							.replace("%cost%",
									ability//
											.getPowerPoints(options) + "")))//
					.build();
			menu.setItem(new MenuItem(addAbility, new MenuClickHandler() {
				@Override
				public void onClick(Player arg0, Inventory arg1, ClickType arg2, ItemStack arg3, int arg4) {
					for (CustomOption option : options.values()) {
						customKit.setOption(ability.getName(), option.getName(), option);
					}
					customKit.addAbility(ability);
					handler.onFinish();
				}
			}), 8);
		}
	}

	public void updateItemCustom() {
		if (ability.getItemOptions().size() > 0) {
			int itemId = 0;
			for (String optionName : ability.getItemOptions()) {
				CustomOption option = getOption(optionName);

				ItemStack item = new ItemBuilder().type(option.getMaterial()).durability(option.getDurability())//
						.name(ChatColor.GOLD + "" + ChatColor.BOLD + ("§%" + ability.getName().toLowerCase() + "_" + optionName.toLowerCase() + "%§").replace("_", "-"))//
						.lore(Arrays.asList(ChatColor.GRAY + ("§%" + ability.getName().toLowerCase() + "_" + optionName.toLowerCase() + "-description%§").replace("_", "-"), " ", "§%click-to-change-item%§"))//
						.build();
				MenuClickHandler handler = new MenuClickHandler() {
					@Override
					public void onClick(Player arg0, Inventory arg1, ClickType arg2, ItemStack arg3, int arg4) {
						ItemFinderMenu.open(player, menu, new ItemFinder() {
							@Override
							public void onSelect(ItemStack item) {
								options.get(optionName).setItem(item);
								updateItemCustom();
								menu.open(player);
							}
						}, 1, null, true);
					}
				};
				menu.setItem(new MenuItem(item, handler), 9 * (1 + get(ability.getItemOptions().size(), itemId)));
				++itemId;
			}
		}
	}

	public void updateTitle() {
		menu.setTitle(NameUtils.formatString(customKit.getName()) + " " + getPowerPoint());
	}

	public void updateOptions() {
		if (ability.getOptions().size() - ability.getItemOptions().size() > 0) {
			int itemId = 0;
			for (String optionName : ability.getOptions().keySet()) {
				CustomOption option = getOption(optionName);
				if (option.isItem())
					continue;
				int slotId = (9 * (1 + get(ability.getOptions().size() - ability.getItemOptions().size(), itemId))) + 4;
				ItemStack item = new ItemBuilder().type(option.getIcon().getType()).durability(option.getIcon().getDurability())//
						.name(ChatColor.RED + "" + ChatColor.BOLD + ("§%" + ability.getName().toLowerCase() + "-" + optionName.toLowerCase() + "%§").replace("_", "-"))//
						.lore(Arrays.asList(ChatColor.GRAY + ("§%" + ability.getName().toLowerCase() + "-" + optionName.toLowerCase() + "-description%§").replace("_", "-"), //
								" ", //
								T.t(GameMain.getPlugin(),//
												BattlePlayer.getLanguage(player.getUniqueId()), "option-info")//
										.replace("%value%", option.getValue() + "")//
										.replace("%min%", option.getMinValue() + "")//
										.replace("%max%", option.getMaxValue() + "")))//
						.build();
				menu.setItem(item, slotId);
				if (option.getValue() < option.getMaxValue()) {
					ItemStack next = new ItemBuilder().type(Material.ARROW)//
							.name(getColor(true, option.getMultiplier()) + "" + ChatColor.BOLD + "§%increase-custom-ability%§")//
							.lore("§%click-to-increase%§")//
							.build();
					menu.setItem(new MenuItem(next, new MenuClickHandler() {

						@Override
						public void onClick(Player arg0, Inventory arg1, ClickType arg2, ItemStack arg3, int arg4) {
							options.get(optionName).setValue(option.getValue() + 1);
							updateOptions();
							updateTitle();
							updateEditItem();
							menu.open(player);
						}
					}), slotId + 1);
				} else {
					menu.setItem(new MenuItem(new ItemStack(Material.AIR)), slotId + 1);
				}
				if (option.getValue() > option.getMinValue()) {
					ItemStack previuous = new ItemBuilder().type(Material.ARROW)//
							.name(getColor(false, option.getMultiplier()) + "" + ChatColor.BOLD + "§%decrease-custom-ability%§")//
							.lore("§%click-to-decrease%§")//
							.build();
					menu.setItem(new MenuItem(previuous, new MenuClickHandler() {

						@Override
						public void onClick(Player arg0, Inventory arg1, ClickType arg2, ItemStack arg3, int arg4) {
							options.get(optionName).setValue(option.getValue() - 1);
							updateOptions();
							updateTitle();
							updateEditItem();
							menu.open(player);
						}
					}), slotId - 1);
				} else {
					menu.setItem(new ItemStack(Material.AIR), slotId - 1);
				}
				++itemId;
			}
		} else {
			menu.setItem(new ItemBuilder().type(Material.PAINTING).name("§c§lOps!").lore(Arrays.asList("§%ability-dont-have-options%§")).build(), 31);
		}
	}

	private ChatColor getColor(boolean increase, int multiplier) {
		if (multiplier > 0) {
			if (increase)
				return ChatColor.GREEN;
			else
				return ChatColor.RED;
		} else if (multiplier < 0) {
			if (increase)
				return ChatColor.RED;
			else
				return ChatColor.GREEN;
		}
		return ChatColor.RESET;
	}

	public CustomOption getOption(String optionName) {
		if (!options.containsKey(optionName))
			options.put(optionName, ability.getOption(optionName).clone());
		return ability.getOption(optionName).clone().copy(options.get(optionName)).clone();
	}

	private int get(int size, int id) {
		int start = 0;
		int multiply = 0;
		switch (size) {
		case 1:
			start = 2;
			multiply = 1;
			break;
		case 2:
			start = 1;
			multiply = 2;
			break;
		case 3:
			start = 1;
			multiply = 1;
			break;
		default:
			break;
		}
		return start + (id * multiply);
	}
}
