package br.com.battlebits.game.games.hungergames.inventory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.com.battlebits.commons.core.translate.T;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import br.com.battlebits.commons.api.item.ItemBuilder;
import br.com.battlebits.commons.api.menu.ClickType;
import br.com.battlebits.commons.api.menu.MenuClickHandler;
import br.com.battlebits.commons.api.menu.MenuInventory;
import br.com.battlebits.commons.api.menu.MenuItem;
import br.com.battlebits.commons.core.account.BattlePlayer;
import br.com.battlebits.commons.core.translate.Translate;
import br.com.battlebits.game.GameMain;
import br.com.battlebits.game.constructor.Gamer;
import br.com.battlebits.game.constructor.Kit;
import br.com.battlebits.game.manager.KitManager;
import br.com.battlebits.game.util.NameUtils;

public class KitSelectorMenu {
	private static int itemsPerPage = 21;

	public static void open(Player player, int page) {
		Gamer gamer = Gamer.getGamer(player);
		List<Kit> kits = new ArrayList<>(KitManager.getKits().values());
		Collections.sort(kits, new Comparator<Kit>() {
			@Override
			public int compare(Kit o1, Kit o2) {
				return o1.getName().compareTo(o2.getName());
			}

		});
		MenuInventory menu = new MenuInventory("§%your_Kits%§ [" + page + "/" + ((int) Math.ceil(kits.size() / itemsPerPage) + 1) + "]", 6, true);
		ItemStack nullItem = new ItemBuilder().type(Material.STAINED_GLASS_PANE).durability(15).name(" ").build();

		List<MenuItem> items = new ArrayList<>();
		for (int i = 0; i < kits.size(); i++) {
			Kit kit = kits.get(i);
			if (gamer.hasKit(kit.getName())) {
				items.add(new MenuItem(new ItemBuilder().lore(kit.getDescription()).type(kit.getIcon().getType()).durability(kit.getIcon().getDurability()).name("Kit " + NameUtils.formatString(kit.getName())).build(), new OpenKitMenu(kit)));
			} else {
				ItemStack item = new ItemBuilder().type(Material.STAINED_GLASS_PANE).durability(14)//
						.name(T.t(GameMain.getPlugin(),BattlePlayer.getLanguage(player.getUniqueId()), "kit-inventory-dont-have").replace("%kit%", NameUtils.formatString(kit.getName())))//
						.lore(Arrays.asList("§%ability-inventory-click-buy%§")).build();
				items.add(new MenuItem(item, new OpenKitMenu(kit)));
			}
		}
		// PAGINA§§O
		int pageStart = 0;
		int pageEnd = itemsPerPage;
		if (page > 1) {
			pageStart = ((page - 1) * itemsPerPage);
			pageEnd = (page * itemsPerPage);
		}
		if (pageEnd > items.size()) {
			pageEnd = items.size();
		}
		if (page == 1) {
			menu.setItem(new ItemBuilder().type(Material.INK_SACK).durability(8).name("§%page-last-dont-have%§").build(), 27);
		} else {
			menu.setItem(new MenuItem(new ItemBuilder().type(Material.INK_SACK).durability(10).name("§%page-last-page%§").lore(Arrays.asList("§%page-last-click-here%§")).build(), new MenuClickHandler() {
				@Override
				public void onClick(Player arg0, Inventory arg1, ClickType arg2, ItemStack arg3, int arg4) {
					new CustomKitSelectorMenu(arg0, page - 1);
				}
			}), 27);
		}

		if (Math.ceil(items.size() / itemsPerPage) + 1 > page) {
			menu.setItem(new MenuItem(new ItemBuilder().type(Material.INK_SACK).durability(10).name("§%page-next-page%§").lore(Arrays.asList("§%page-next-click-here%§")).build(), new MenuClickHandler() {
				@Override
				public void onClick(Player arg0, Inventory arg1, ClickType arg2, ItemStack arg3, int arg4) {
					new CustomKitSelectorMenu(arg0, page + 1);
				}
			}), 35);
		} else {
			menu.setItem(new ItemBuilder().type(Material.INK_SACK).durability(8).name("§%page-next-dont-have%§").build(), 35);
		}

		// CUSTOM KITS

		int w = 19;

		for (int i = pageStart; i < pageEnd; i++) {
			MenuItem item = items.get(i);
			menu.setItem(item, w);
			if (w % 9 == 7) {
				w += 3;
				continue;
			}
			w += 1;
		}
		if (items.size() == 0) {
			menu.setItem(new ItemBuilder().type(Material.PAINTING).name("§c§lOps!").lore(Arrays.asList("§%error%§")).build(), 31);
		}

		for (int i = 0; i < 9; i++) {
			if (menu.getItem(i) == null)
				menu.setItem(nullItem, i);
		}
		menu.open(player);
	}

	private static boolean right = false;

	public static class OpenKitMenu implements MenuClickHandler {

		private Kit kit;

		public OpenKitMenu(Kit kit) {
			this.kit = kit;
		}

		@Override
		public void onClick(Player arg0, Inventory arg1, ClickType arg2, ItemStack arg3, int arg4) {
			if (ClickType.RIGHT == arg2 && right) {
				// TODO Open Invetory
				return;
			}
			GameMain.getPlugin().getKitManager().selectKit(arg0, kit);
		}

	}
}
