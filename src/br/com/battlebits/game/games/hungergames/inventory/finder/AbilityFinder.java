package br.com.battlebits.game.games.hungergames.inventory.finder;

import br.com.battlebits.game.constructor.Ability;

public interface AbilityFinder {
	public void onAbilityFound(Ability ability);
}
