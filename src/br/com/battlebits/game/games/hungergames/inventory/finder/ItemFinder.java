package br.com.battlebits.game.games.hungergames.inventory.finder;

import org.bukkit.inventory.ItemStack;

public interface ItemFinder {
	public void onSelect(ItemStack item);
}
