package br.com.battlebits.game.games.hungergames.listener;

import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

import br.com.battlebits.game.GameMain;
import br.com.battlebits.game.games.hungergames.HungerGamesMode;
import br.com.battlebits.game.games.hungergames.structure.FeastStructure;
import br.com.battlebits.game.stage.GameStage;

public class BlockListener extends br.com.battlebits.game.listener.GameListener {

	public BlockListener(GameMain main) {
		super(main);
	}

	@EventHandler
	public void onBreak(BlockBreakEvent event) {
		if (isFeastBlock(event.getBlock()))
			event.setCancelled(true);
	}

	@EventHandler
	public void onPlace(BlockPlaceEvent event) {
		if (isFeastBlock(event.getBlock()))
			event.setCancelled(true);
	}

	private boolean isFeastBlock(Block b) {
		return FeastStructure.isFeastBlock(b) && getGameMain().getGameStage() == GameStage.GAMETIME && getGameMain().getTimer() < HungerGamesMode.FEAST_SPAWN;
	}

}
