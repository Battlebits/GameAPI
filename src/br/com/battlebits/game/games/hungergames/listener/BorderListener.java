package br.com.battlebits.game.games.hungergames.listener;

import java.util.HashMap;
import java.util.Map;

import br.com.battlebits.commons.core.translate.T;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

import br.com.battlebits.commons.bukkit.event.update.UpdateEvent;
import br.com.battlebits.commons.bukkit.event.update.UpdateEvent.UpdateType;
import br.com.battlebits.commons.core.account.BattlePlayer;
import br.com.battlebits.commons.core.translate.Translate;
import br.com.battlebits.game.GameMain;
import br.com.battlebits.game.stage.GameStage;

public class BorderListener extends br.com.battlebits.game.listener.GameListener {

	public Map<Player, Location> locations = new HashMap<>();

	public BorderListener(GameMain main) {
		super(main);
	}

	@EventHandler
	public void onTime(UpdateEvent event) {
		if (event.getType() != UpdateType.SECOND)
			return;
		for (Player p : Bukkit.getOnlinePlayers()) {
			if (isOnWarning(p)) {
				if (!GameStage.isPregame(getGameMain().getGameStage())) {
					p.sendMessage(T.t(GameMain.getPlugin(),BattlePlayer.getLanguage(p.getUniqueId()), "near-border"));
					continue;
				}
			}
			if (isNotInBoard(p) || p.getLocation().getY() > 129) {
				if (GameStage.isPregame(getGameMain().getGameStage())) {
					if (locations.containsKey(p)) {
						p.teleport(locations.get(p));
						continue;
					}
				} else {
					p.sendMessage(T.t(GameMain.getPlugin(),BattlePlayer.getLanguage(p.getUniqueId()), "out-border"));
					@SuppressWarnings("deprecation")
					EntityDamageEvent e = new EntityDamageEvent((Entity) p, DamageCause.CUSTOM, 4.0d);
					if (e.isCancelled()) {
						e.setCancelled(false);
					}
					p.setLastDamageCause(e);
					p.damage(4.0);
				}
			}
			locations.put(p, p.getLocation());
		}
	}

	private boolean isNotInBoard(Player p) {
		int size = (int) 1000 / 2;
		return ((p.getLocation().getBlockX() > size) || (p.getLocation().getBlockX() < -size)
				|| (p.getLocation().getBlockZ() > size) || (p.getLocation().getBlockZ() < -size));
	}

	private boolean isOnWarning(Player p) {
		int size = (int) 1000 / 2;
		size = size - 20;
		return !isNotInBoard(p) && ((p.getLocation().getBlockX() > size) || (p.getLocation().getBlockX() < -size)
				|| (p.getLocation().getBlockZ() > size) || (p.getLocation().getBlockZ() < -size));
	}

}