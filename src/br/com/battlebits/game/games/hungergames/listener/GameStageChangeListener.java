package br.com.battlebits.game.games.hungergames.listener;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;

import br.com.battlebits.game.GameMain;
import br.com.battlebits.game.event.game.GameStageChangeEvent;
import br.com.battlebits.game.games.hungergames.schedule.GameScheduler;
import br.com.battlebits.game.games.hungergames.schedule.PregameScheduler;
import br.com.battlebits.game.listener.GameListener;
import br.com.battlebits.game.stage.GameStage;

public class GameStageChangeListener extends GameListener {

	public GameStageChangeListener(GameMain main) {
		super(main);
	}

	@EventHandler
	public void onGameStageChange(GameStageChangeEvent event) {
		if (event.getLastStage() == GameStage.WAITING) {
			if (event.getNewStage() == GameStage.PREGAME)
				getGameMain().getSchedulerManager().addScheduler("pregamer", new PregameScheduler());
		} else if (event.getNewStage() == GameStage.WAITING) {
			getGameMain().getSchedulerManager().cancelScheduler("pregamer");
			if (event.getLastStage() == GameStage.STARTING)
				Bukkit.broadcastMessage("§%players-not-enought%§");
		} else if (event.getLastStage() == GameStage.INVINCIBILITY) {
			getGameMain().getSchedulerManager().cancelScheduler("invincibility");
			getGameMain().getSchedulerManager().addScheduler("gametime", new GameScheduler());
			Bukkit.broadcastMessage("§%invincibility-end%§");
		}
		System.out.println(event.getLastStage().toString() + " > " + event.getNewStage().toString());
	}

}
