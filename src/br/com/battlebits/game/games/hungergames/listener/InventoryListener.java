package br.com.battlebits.game.games.hungergames.listener;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

import br.com.battlebits.commons.BattlebitsAPI;
import br.com.battlebits.commons.api.item.ActionItemStack;
import br.com.battlebits.commons.api.item.ActionItemStack.InteractHandler;
import br.com.battlebits.commons.api.item.ItemBuilder;
import br.com.battlebits.commons.core.permission.Group;
import br.com.battlebits.game.GameMain;
import br.com.battlebits.game.event.game.GameStageChangeEvent;
import br.com.battlebits.game.event.game.GameStartEvent;
import br.com.battlebits.game.games.hungergames.inventory.CustomKitSelectorMenu;
import br.com.battlebits.game.games.hungergames.inventory.KitSelectorMenu;
import br.com.battlebits.game.stage.GameStage;

public class InventoryListener extends br.com.battlebits.game.listener.GameListener {
	private ActionItemStack kitSelector = new ActionItemStack(new ItemBuilder().type(Material.CHEST).name("§%kitSelector-item-name%§")
			.lore("§%kitSelector-item-lore%§").build(), new InteractHandler() {
				
				@Override
				public boolean onInteract(Player player, ItemStack item, Action action) {
					new KitSelectorMenu(player, 1);
					return false;
				}
			});
	private ActionItemStack customKit = new ActionItemStack(new ItemBuilder().type(Material.WORKBENCH).name("§%customKitSelector-item-name%§")
			.lore("§%customKitSelector-item-lore%§").build(), new InteractHandler() {
				
				@Override
				public boolean onInteract(Player player, ItemStack item, Action action) {
					new CustomKitSelectorMenu(player, 1);
					return false;
				}
			});
	private ActionItemStack cosmetics = new ActionItemStack(new ItemBuilder().type(Material.ENDER_CHEST).name("§%cosmetics-item-name%§")
			.lore("§%cosmetics-item-lore%§").build(), new InteractHandler() {
				
				@Override
				public boolean onInteract(Player player, ItemStack item, Action action) {
					if (BattlebitsAPI.getAccountCommon().getBattlePlayer(player.getUniqueId())
							.hasGroupPermission(Group.LIGHT)) {
						if (getGameMain().getTimer() < 15) {
							player.sendMessage("§%cosmetics-game-is-starting%§");
							return false;
						}
						// TODO Open
					} else {
						player.sendMessage("§%must-be-vip%§");
					}
					return false;
				}
			});
	private ActionItemStack lobby = new ActionItemStack(new ItemBuilder().type(Material.WATCH).name("§%lobby-item-name%§")
			.lore("§%lobby-item-lore%§").build(), new InteractHandler() {
				
				@Override
				public boolean onInteract(Player player, ItemStack item, Action action) {
					ByteArrayDataOutput out = ByteStreams.newDataOutput();
					out.writeUTF("Lobby");
					player.sendPluginMessage(getGameMain(), "BungeeCord", out.toByteArray());
					return false;
				}
			});

	public InventoryListener(GameMain main) {
		super(main);
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		ItemStack profile = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
		SkullMeta skullMeta = (SkullMeta) profile.getItemMeta();
		skullMeta.setOwner(event.getPlayer().getName());
		skullMeta.setDisplayName("§%profile-item-name%§");
		skullMeta.setLore(Arrays.asList("§%profile-item-lore%§"));
		profile.setItemMeta(skullMeta);
		ActionItemStack action = new ActionItemStack(profile, new InteractHandler() {
			
			@Override
			public boolean onInteract(Player player, ItemStack item, Action action) {
				event.getPlayer().sendMessage("§%soon%§");
				return false;
			}
		});
		event.getPlayer().getInventory().setItem(0, kitSelector.getItemStack());
		event.getPlayer().getInventory().setItem(1, customKit.getItemStack());
		event.getPlayer().getInventory().setItem(3, action.getItemStack());
		event.getPlayer().getInventory().setItem(7, cosmetics.getItemStack());
		event.getPlayer().getInventory().setItem(8, lobby.getItemStack());
	}

	@EventHandler
	public void onGameChange(GameStageChangeEvent event) {
		if (event.getNewStage() == GameStage.STARTING) {
			for (Player p : Bukkit.getOnlinePlayers()) {
				for (ItemStack item : p.getInventory().getContents()) {
					if (item == null)
						continue;
					if (isEqual(item, cosmetics.getItemStack())) {
						p.getInventory().remove(item);
					}
				}
				if (isEqual(p.getItemOnCursor(), cosmetics.getItemStack()))
					p.setItemOnCursor(null);
				// Remove Cosmetics
			}
		} else if (event.getNewStage() == GameStage.WAITING && event.getLastStage() == GameStage.STARTING) {
			for (Player p : Bukkit.getOnlinePlayers()) {
				int i = 3;
				while (p.getInventory().getItem(i) != null) {
					i++;
				}
				p.getInventory().setItem(i, cosmetics.getItemStack());
			}
		}
	}

	private boolean isEqual(ItemStack item, ItemStack item2) {
		if (item.getType() != item2.getType())
			return false;
		if (!item.hasItemMeta()) {
			if (item2.hasItemMeta())
				return false;
			return true;
		}
		if (!item.getItemMeta().hasDisplayName()) {
			if (item2.getItemMeta().hasDisplayName())
				return false;
			return true;
		}
		if (!item.getItemMeta().getDisplayName().equals(item2.getItemMeta().getDisplayName()))
			return false;
		return true;
	}

	@EventHandler
	public void onGameStart(GameStartEvent event) {
		HandlerList.unregisterAll(this);
	}

}
