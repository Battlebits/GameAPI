package br.com.battlebits.game.games.hungergames.listener;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import br.com.battlebits.commons.bukkit.event.admin.PlayerAdminModeEvent;
import br.com.battlebits.commons.bukkit.event.update.UpdateEvent;
import br.com.battlebits.commons.bukkit.event.update.UpdateEvent.UpdateType;
import br.com.battlebits.game.GameMain;
import br.com.battlebits.game.event.game.GameTimerEvent;
import br.com.battlebits.game.event.player.PlayerSelectedKitEvent;
import br.com.battlebits.game.games.hungergames.HungerGamesMode;

public class ScoreboardListener extends br.com.battlebits.game.listener.GameListener {

	private HungerGamesMode mode;

	public ScoreboardListener(GameMain main, HungerGamesMode mode) {
		super(main);
		this.mode = mode;
	}

	private int i = 0;

	@EventHandler
	public void onUpdate(UpdateEvent event) {
		if (event.getType() != UpdateType.TICK)
			return;
		++i;
		if (i % 7 == 0) {
			i = 0;
			mode.getScoreBoardManager().updateTitleText();
			for (Player p : Bukkit.getOnlinePlayers()) {
				mode.getScoreBoardManager().updateTitle(p);
			}
		}
	}

	@EventHandler
	public void onPlayerSelectedKit(PlayerSelectedKitEvent event) {
		mode.getScoreBoardManager().updatePlayerKit(event.getPlayer());
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onJoin(PlayerJoinEvent event) {
		mode.getScoreBoardManager().createScoreboard(event.getPlayer());
		mode.getScoreBoardManager().updatePlayersLeft();
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onQuit(PlayerQuitEvent event) {
		mode.getScoreBoardManager().updatePlayersLeft();
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onAdminMode(PlayerAdminModeEvent event) {
		mode.getScoreBoardManager().updatePlayersLeft();
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerDeath(PlayerDeathEvent event) {
		if (event.getEntity().getKiller() != null)
			mode.getScoreBoardManager().updatePlayerKills(event.getEntity().getKiller());
	}

	@EventHandler
	public void onLeave(PlayerQuitEvent event) {
		mode.getScoreBoardManager().updatePlayersLeft();
	}

	@EventHandler
	public void onGameStage(GameTimerEvent event) {
		mode.getScoreBoardManager().updateTimer();
	}

}
