package br.com.battlebits.game.games.hungergames.listener;

import br.com.battlebits.commons.core.translate.T;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import br.com.battlebits.commons.core.account.BattlePlayer;
import br.com.battlebits.commons.core.translate.Translate;
import br.com.battlebits.game.GameMain;
import br.com.battlebits.game.constructor.CustomKit;
import br.com.battlebits.game.constructor.Gamer;
import br.com.battlebits.game.event.player.PlayerSelectKitEvent;
import br.com.battlebits.game.util.NameUtils;

public class SelectKitListener implements Listener {

	@EventHandler
	public void onPlayer(PlayerSelectKitEvent event) {
		if (event.getKit() == null)
			return;
		if (event.getKit() instanceof CustomKit) {
			if (((CustomKit) event.getKit()).getPowerPoints() > 100) {
				event.getPlayer().sendMessage("§%custom-kit-too-over-power%§");
				event.setCancelled(true);
				GameMain.getPlugin().getKitManager().unregisterPlayer(event.getPlayer());
				return;
			}
		}
		if(!Gamer.getGamer(event.getPlayer()).hasKit(event.getKit().getName())) {
			event.getPlayer().sendMessage(T.t(GameMain.getPlugin(),BattlePlayer.getLanguage(event.getPlayer().getUniqueId()), "kit-dont-have").replace("%kitName%", NameUtils.formatString(event.getKit().getName())));
			event.setCancelled(true);
			return;
		}
		event.getPlayer().sendMessage(T.t(GameMain.getPlugin(),BattlePlayer.getLanguage(event.getPlayer().getUniqueId()), "kit-selected-success").replace("%kitName%", NameUtils.formatString(event.getKit().getName())));
	}
}
