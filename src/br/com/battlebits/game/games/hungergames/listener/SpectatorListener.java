package br.com.battlebits.game.games.hungergames.listener;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

import br.com.battlebits.commons.api.vanish.VanishAPI;
import br.com.battlebits.commons.bukkit.event.teleport.PlayerTeleportCommandEvent;
import br.com.battlebits.commons.bukkit.event.teleport.PlayerTeleportCommandEvent.TeleportResult;
import br.com.battlebits.commons.bukkit.event.vanish.PlayerShowToPlayerEvent;
import br.com.battlebits.game.GameMain;
import br.com.battlebits.game.constructor.Gamer;
import br.com.battlebits.game.event.player.PlayerSpectateEvent;
import br.com.battlebits.game.games.hungergames.HungerGamesMode;
import br.com.battlebits.game.listener.GameListener;

public class SpectatorListener extends GameListener {

	public SpectatorListener(GameMain main) {
		super(main);
	}

	@EventHandler
	public void onSpectate(PlayerSpectateEvent event) {
		Player p = event.getPlayer();
		if (p == null)
			return;
		for (Player online : Bukkit.getOnlinePlayers())
			if (online != null)
				if (online.isOnline())
					if (online.getUniqueId() != p.getUniqueId())
						if (online.canSee(p))
							online.hidePlayer(p);
		VanishAPI.getInstance().hidePlayer(p);
		p.setGameMode(GameMode.ADVENTURE);
		p.setAllowFlight(true);
		p.setFlying(true);
		p.sendMessage("§%spectator-mode-on%§");
		((HungerGamesMode) GameMain.getPlugin().getGameMode()).getScoreBoardManager().updatePlayersLeft();
	}

	@EventHandler
	public void onVisible(PlayerShowToPlayerEvent event) {
		if (!Gamer.getGamer(event.getPlayer()).isSpectator())
			return;
		Gamer toGamer = Gamer.getGamer(event.getToPlayer());
		if (!toGamer.isNotPlaying()) {
			if (!toGamer.isSpectatorsEnabled())
				event.setCancelled(true);
		}
	}

	@EventHandler
	public void onTeleport(PlayerTeleportCommandEvent event) {
		if (event.getResult() != TeleportResult.NO_PERMISSION)
			return;
		if (Gamer.getGamer(event.getPlayer()).isSpectator())
			event.setResult(TeleportResult.ONLY_PLAYER_TELEPORT);
	}

}
