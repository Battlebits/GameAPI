package br.com.battlebits.game.games.hungergames.listener;

import org.bukkit.event.EventHandler;

import br.com.battlebits.commons.core.data.DataServer;
import br.com.battlebits.commons.core.server.loadbalancer.server.MinigameState;
import br.com.battlebits.game.GameMain;
import br.com.battlebits.game.event.game.GameStageChangeEvent;
import br.com.battlebits.game.event.game.GameTimerEvent;
import br.com.battlebits.game.listener.GameListener;
import br.com.battlebits.game.stage.GameStage;

public class UpdateListener extends GameListener {

	public UpdateListener(GameMain main) {
		super(main);
	}

	@EventHandler
	public void onStart(GameStageChangeEvent event) {
		if (event.getNewStage() == GameStage.WAITING) {
			DataServer.updateStatus(MinigameState.WAITING, GameMain.getPlugin().getTimer());
		} else if (GameStage.isPregame(event.getNewStage())) {
			DataServer.updateStatus(MinigameState.PREGAME, GameMain.getPlugin().getTimer());
		} else if (GameStage.isInvincibility(event.getNewStage())) {
			DataServer.updateStatus(MinigameState.INVINCIBILITY, GameMain.getPlugin().getTimer());
		} else {
			DataServer.updateStatus(MinigameState.GAMETIME, GameMain.getPlugin().getTimer());
		}
	}

	@EventHandler
	public void onChange(GameTimerEvent event) {
		DataServer.updateStatus(MinigameState.valueOf(GameMain.getPlugin().getGameStage().toString()),
				GameMain.getPlugin().getTimer());
	}

}
