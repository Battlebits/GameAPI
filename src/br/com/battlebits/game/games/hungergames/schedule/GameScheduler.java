package br.com.battlebits.game.games.hungergames.schedule;

import java.util.HashMap;
import java.util.Random;

import br.com.battlebits.commons.core.translate.T;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import br.com.battlebits.commons.BattlebitsAPI;
import br.com.battlebits.commons.api.admin.AdminMode;
import br.com.battlebits.commons.core.account.BattlePlayer;
import br.com.battlebits.commons.core.permission.Group;
import br.com.battlebits.commons.core.translate.Translate;
import br.com.battlebits.game.GameMain;
import br.com.battlebits.game.constructor.CustomKit;
import br.com.battlebits.game.constructor.Gamer;
import br.com.battlebits.game.constructor.ScheduleArgs;
import br.com.battlebits.game.games.hungergames.HungerGamesMode;
import br.com.battlebits.game.games.hungergames.listener.DeathListener;
import br.com.battlebits.game.games.hungergames.manager.FeastManager;
import br.com.battlebits.game.games.hungergames.structure.FinalBattleStructure;
import br.com.battlebits.game.games.hungergames.structure.MinifeastStructure;
import br.com.battlebits.game.games.hungergames.translate.MessageManager;
import br.com.battlebits.game.games.hungergames.translate.MessageManager.CountDownMessageType;
import br.com.battlebits.game.scheduler.Schedule;
import br.com.battlebits.game.util.NameUtils;

public class GameScheduler implements Schedule {
	private static FeastManager feastManager = new FeastManager();
	private MinifeastStructure minifeast;
	private FinalBattleStructure finalBattle;
	private int nextMinifeast;
	private Random r = new Random();

	public GameScheduler() {
		minifeast = new MinifeastStructure();
		finalBattle = new FinalBattleStructure();
		nextMinifeast = 240 + r.nextInt(300);
	}

	private void killPlayer(Player p) {
		Gamer gamer = Gamer.getGamer(p);
		BattlePlayer player = BattlePlayer.getPlayer(p.getUniqueId());
		DeathListener.deathPlayer(p);
		gamer.addDeath();
		HashMap<String, String> replaces = new HashMap<>();
		replaces.put("%player%", p.getName());
		replaces.put("%player_Kit%", (gamer.getKit() != null ? ((gamer.getKit() instanceof CustomKit) ? ChatColor.DARK_GRAY : "") : "") + NameUtils.formatString(gamer.getKitName())); // TODO
		// KITS
		String kickMessage = DeathListener.deathMessage(p, "death-message-kills", replaces);
		if (player.hasGroupPermission(Group.TRIAL)) {
			AdminMode.getInstance().setAdmin(p);
			gamer.setGamemaker(true);
		} else {
			gamer.setSpectator(true);
		}
		if (!player.hasGroupPermission(Group.ULTIMATE)) {
			int number = GameMain.getPlugin().playersLeft() + 1;
			if (number <= 10) {
				p.kickPlayer(T.t(GameMain.getPlugin(),player.getLanguage(), "you-lost-last-10").replace("%number%", number + "").replace("%total%", GameMain.getPlugin().getTotalPlayers() + "") + " " + kickMessage);
				return;
			}
			p.kickPlayer(T.t(GameMain.getPlugin(),player.getLanguage(), "you-lost") + " " + kickMessage);
		}
	}

	@Override
	public void pulse(ScheduleArgs args) {
		/**
		 * FEAST
		 */
		if (args.getTimer() == 60 * 60) {
			Player p = null;
			int kills = 0;
			for (Player player : Bukkit.getOnlinePlayers()) {
				Gamer gamer = Gamer.getGamer(player);
				if (gamer.isGamemaker())
					continue;
				if (gamer.isSpectator())
					continue;
				if (gamer.getMatchkills() >= kills) {
					if (p != null) {
						killPlayer(p);
					}
					kills = gamer.getMatchkills();
					p = player;
				} else {
					killPlayer(player);
				}
			}
			((HungerGamesMode) GameMain.getPlugin().getGameMode()).checkWinner();
		}
		if (!feastManager.isChestSpawned() && args.getTimer() >= HungerGamesMode.FEAST_SPAWN - feastManager.getCounter()) {
			if (feastManager.getCounter() > 0) {
				if (!feastManager.isSpawned()) {
					feastManager.spawnFeast();
					((HungerGamesMode) GameMain.getPlugin().getGameMode()).getScoreBoardManager().addFeastTimer(feastManager.getFeastLocation(), feastManager.getCounter());
				} else {
					((HungerGamesMode) GameMain.getPlugin().getGameMode()).getScoreBoardManager().updateFeastTimer(feastManager.getCounter());
				}
				if ((feastManager.getCounter() % 60 == 0 || (feastManager.getCounter() < 60 && (feastManager.getCounter() % 15 == 0 || feastManager.getCounter() == 10 || feastManager.getCounter() <= 5))))
					for (Player p : Bukkit.getOnlinePlayers()) {
						p.sendMessage(MessageManager.getCountDownMessage(CountDownMessageType.FEAST, BattlebitsAPI.getAccountCommon().getBattlePlayer(p.getUniqueId()).getLanguage(), feastManager.getCounter(), feastManager.getFeastLocation()));
					}
			} else {
				((HungerGamesMode) GameMain.getPlugin().getGameMode()).getScoreBoardManager().removeFeastTimer(feastManager.getFeastLocation());
				feastManager.spawnChests();
				for (Player p : Bukkit.getOnlinePlayers()) {
					p.sendMessage(MessageManager.getCountDownMessage(CountDownMessageType.FEAST_SPAWNED, BattlebitsAPI.getAccountCommon().getBattlePlayer(p.getUniqueId()).getLanguage(), feastManager.getCounter(), feastManager.getFeastLocation()));
				}
			}
			feastManager.count();
		}
		/**
		 * MINIFEAST
		 */
		if (nextMinifeast <= 0) {
			nextMinifeast = 240 + r.nextInt(300);
			Location place = minifeast.findPlace();
			minifeast.place(place);
			for (Player p : Bukkit.getOnlinePlayers()) {
				p.sendMessage(MessageManager.getMinifeastMessage(BattlePlayer.getLanguage(p.getUniqueId()), place));
			}
		} else {
			--nextMinifeast;
		}
		/**
		 * BONUS FEAST
		 */
		if (args.getTimer() == HungerGamesMode.BONUSFEAST_SPAWN) {
			feastManager.spawnBonusFeast();
			Bukkit.broadcastMessage("§%bonusfeast-spawned%§");
		}
		/**
		 * FINAL BATTLE
		 */
		if (args.getTimer() == HungerGamesMode.FINALBATTLE_TIME) {
			Location loc = finalBattle.findPlace();
			finalBattle.place(loc);
			finalBattle.teleportPlayers(loc);
			Bukkit.broadcastMessage("§%finalbattle-spawned%§");
		}

	}

	public static FeastManager getFeastManager() {
		return feastManager;
	}

}
