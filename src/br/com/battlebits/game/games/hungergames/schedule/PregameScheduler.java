package br.com.battlebits.game.games.hungergames.schedule;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import br.com.battlebits.commons.BattlebitsAPI;
import br.com.battlebits.commons.bukkit.listener.AntiAFK;
import br.com.battlebits.game.GameMain;
import br.com.battlebits.game.constructor.ScheduleArgs;
import br.com.battlebits.game.games.hungergames.HungerGamesMode;
import br.com.battlebits.game.games.hungergames.manager.ColiseumManager;
import br.com.battlebits.game.games.hungergames.translate.MessageManager;
import br.com.battlebits.game.games.hungergames.translate.MessageManager.CountDownMessageType;
import br.com.battlebits.game.scheduler.Schedule;
import br.com.battlebits.game.stage.GameStage;

public class PregameScheduler implements Schedule {

	private ColiseumManager manager;

	public PregameScheduler() {
		manager = new ColiseumManager();
	}

	@Override
	public void pulse(ScheduleArgs args) {
		if (!(args.getStage() == GameStage.PREGAME || args.getStage() == GameStage.STARTING))
			return;
		for (Player p : Bukkit.getOnlinePlayers()) {
			if (!ColiseumManager.isInsideColiseum(p)) {
				if (AntiAFK.getTime(p) >= 10) {
					HungerGamesMode.teleportToSpawn(p);
					if (manager.isConstructed()) {
						p.sendMessage("§%afk-out-of-coliseum%§");
					} else {
						p.sendMessage("§%afk-out-of-spawn%§");
					}
				}
			}
		}
		if (args.getTimer() <= 60) {
			if (args.getTimer() > 30) {
				manager.teleportRecursive(args.getTimer() - 30);
			} else {
				manager.teleportOutsidePlayers();
			}
			if (manager.isDoorsOpen())
				manager.closeDoors();
		} else {
			if (!manager.isDoorsOpen())
				manager.openDoors();
		}
		if (args.getTimer() <= 15 && args.getStage() == GameStage.PREGAME) {
			GameMain.getPlugin().setGameStage(GameStage.STARTING);
			GameMain.getPlugin().setTimer(args.getTimer());
		} else if (args.getTimer() <= 0) {
			if (GameMain.getPlugin().playersLeft() >= 1) {
				GameMain.getPlugin().startGame();
			} else {
				GameMain.getPlugin().setGameStage(GameStage.WAITING);
			}
			return;
		}
		if (args.getTimer() <= 5) {
			for (Player p : Bukkit.getOnlinePlayers()) {
				p.playSound(p.getLocation(), Sound.NOTE_STICKS, 1f, 1f);
			}
		}
		if ((args.getTimer() % 60 == 0 || (args.getTimer() < 60 && (args.getTimer() % 15 == 0 || args.getTimer() == 10 || args.getTimer() <= 5)))) {
			for (Player p : Bukkit.getOnlinePlayers()) {
				p.sendMessage(MessageManager.getCountDownMessage(CountDownMessageType.GAMESTART, BattlebitsAPI.getAccountCommon().getBattlePlayer(p.getUniqueId()).getLanguage(), args.getTimer()));
			}
		}
	}

}
