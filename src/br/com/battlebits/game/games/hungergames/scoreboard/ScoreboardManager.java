package br.com.battlebits.game.games.hungergames.scoreboard;

import br.com.battlebits.commons.core.translate.T;
import br.com.battlebits.game.GameMain;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;

import br.com.battlebits.commons.BattlebitsAPI;
import br.com.battlebits.commons.bukkit.scoreboard.ScoreboardAPI;
import br.com.battlebits.commons.core.account.BattlePlayer;
import br.com.battlebits.commons.core.translate.Language;
import br.com.battlebits.commons.core.translate.Translate;
import br.com.battlebits.commons.util.string.StringHourTime;
import br.com.battlebits.commons.util.string.StringScroller;
import br.com.battlebits.game.constructor.Gamer;
import br.com.battlebits.game.games.hungergames.HungerGamesMode;
import br.com.battlebits.game.games.hungergames.schedule.GameScheduler;
import br.com.battlebits.game.util.NameUtils;

public class ScoreboardManager implements br.com.battlebits.game.manager.ScoreboardManager {

	private HungerGamesMode mode;
	private StringScroller scroller;
	private String title;

	public ScoreboardManager(HungerGamesMode mode) {
		this.mode = mode;
		scroller = new StringScroller("CustomHG        " + BattlebitsAPI.getServerId(), 12, 8);
		title = scroller.next();
	}

	public void createScoreboard(Player player) {
		Gamer gamer = Gamer.getGamer(player);
		Language lang = BattlePlayer.getLanguage(player.getUniqueId());
		ScoreboardAPI.createObjectiveIfNotExistsToPlayer(player, "main", "§8§l>> §6§l" + title + " §8§l<<",
				DisplaySlot.SIDEBAR);
		addScore(player, "v3", 8, "");
		addScore(player, "timer", 7, T.t(GameMain.getPlugin(),lang, "scoreboard-starts-in")
				+ StringHourTime.getHourTime(mode.getGameMain().getTimer()));
		addScore(player, "players", 6, T.t(GameMain.getPlugin(),lang, "scoreboard-players")
				+ mode.getGameMain().playersLeft() + "/" + mode.getGameMain().getTotalPlayers());
		addScore(player, "v2", 5, "");
		addScore(player, "kit", 4, T.t(GameMain.getPlugin(),lang, "scoreboard-kit")
				+ NameUtils.formatString(Gamer.getGamer(player).getKitName()));
		addScore(player, "kills", 3, T.t(GameMain.getPlugin(),lang, "scoreboard-kills") + gamer.getMatchkills());
		addScore(player, "v1", 2, "");
		addScore(player, "site", 1, "§6" + BattlebitsAPI.WEBSITE);
		if (GameScheduler.getFeastManager().isSpawned()) {
			addFeastTimer(player, GameScheduler.getFeastManager().getFeastLocation(),
					GameScheduler.getFeastManager().getCounter());
			if (GameScheduler.getFeastManager().isChestSpawned()) {
				removeFeastTimer(player, GameScheduler.getFeastManager().getFeastLocation());
			}
		}

	}

	public void updateTitleText() {
		title = scroller.next();
	}

	public void updateTitle(Player p) {
		ScoreboardAPI.setObjectiveDisplayNameToPlayer(p, "main", "§8§l>> §6§l" + title + " §8§l<<");
	}

	public void updateTimer() {
		for (Player p : Bukkit.getOnlinePlayers()) {
			updateTimer(p);
		}
	}

	public void updateTimer(Player p) {
		Language lang = BattlePlayer.getLanguage(p.getUniqueId());
		String str = T.t(GameMain.getPlugin(),lang, "scoreboard-starts-in");
		switch (mode.getGameMain().getGameStage()) {
		case FINAL:
		case GAMETIME:
		case WINNER:
			str = T.t(GameMain.getPlugin(),lang, "scoreboard-current-time");
			break;
		case INVINCIBILITY:
			str = T.t(GameMain.getPlugin(),lang, "scoreboard-invincibility");
			break;
		default:
			break;
		}
		updateScore(p, "timer", str + StringHourTime.getHourTime(mode.getGameMain().getTimer()));
	}

	public void addFeastTimer(Location loc, int time) {
		for (Player p : Bukkit.getOnlinePlayers()) {
			addFeastTimer(p, loc, time);
		}
	}

	public void addFeastTimer(Player player, Location loc, int time) {
		Language lang = BattlePlayer.getLanguage(player.getUniqueId());
		addScore(player, "feastTimer", 7,
				T.t(GameMain.getPlugin(),lang, "scoreboard-feastTimer") + StringHourTime.getHourTime(time));
		addScore(player, "feastLocation", 6, T.t(GameMain.getPlugin(),lang, "scoreboard-feastLocation")
				+ loc.getBlockX() + ", " + loc.getBlockY() + ", " + loc.getBlockZ());
		addScore(player, "timer", 10, T.t(GameMain.getPlugin(),lang, "scoreboard-current-time")
				+ StringHourTime.getHourTime(mode.getGameMain().getTimer()));
		addScore(player, "players", 9, T.t(GameMain.getPlugin(),lang, "scoreboard-players")
				+ mode.getGameMain().playersLeft() + "/" + mode.getGameMain().getTotalPlayers());
		addScore(player, "v4", 11, "");
	}

	public void updateFeastTimer(int time) {
		for (Player p : Bukkit.getOnlinePlayers()) {
			updateFeastTimer(p, time);
		}
	}

	public void updateFeastTimer(Player player, int time) {
		updateScore(player, "feastTimer",
				T.t(GameMain.getPlugin(),BattlePlayer.getLanguage(player.getUniqueId()), "scoreboard-feastTimer")
						+ StringHourTime.getHourTime(time));
	}

	public void removeFeastTimer(Location loc) {
		for (Player p : Bukkit.getOnlinePlayers()) {
			removeFeastTimer(p, loc);
		}
	}

	public void removeFeastTimer(Player player, Location loc) {
		Language lang = BattlePlayer.getLanguage(player.getUniqueId());
		updateScore(player, "feastLocation", T.t(GameMain.getPlugin(),lang, "scoreboard-feastTimer") + loc.getBlockX()
				+ ", " + loc.getBlockY() + ", " + loc.getBlockZ());
		addScore(player, "v6", 7, "");
		addScore(player, "timer", 9, T.t(GameMain.getPlugin(),lang, "scoreboard-current-time")
				+ StringHourTime.getHourTime(mode.getGameMain().getTimer()));
		addScore(player, "players", 8, T.t(GameMain.getPlugin(),lang, "scoreboard-players")
				+ mode.getGameMain().playersLeft() + "/" + mode.getGameMain().getTotalPlayers());
		addScore(player, "v5", 10, "");
		player.getScoreboard().resetScores("§1§1");
	}

	public void updatePlayerKit(Player player) {
		updateScore(player, "kit",
				T.t(GameMain.getPlugin(),BattlePlayer.getLanguage(player.getUniqueId()), "scoreboard-kit")
						+ NameUtils.formatString(Gamer.getGamer(player).getKitName()));
	}

	public void updatePlayerKills(Player player) {
		updateScore(player, "kills",
				T.t(GameMain.getPlugin(),BattlePlayer.getLanguage(player.getUniqueId()), "scoreboard-kills")
						+ Gamer.getGamer(player).getMatchkills());
	}
	
	public void updatePlayersLeft() {
		for (Player p : Bukkit.getOnlinePlayers()) {
			updateScore(p, "players",
					T.t(GameMain.getPlugin(),BattlePlayer.getLanguage(p.getUniqueId()), "scoreboard-players")
							+ mode.getGameMain().playersLeft() + "/" + mode.getGameMain().getTotalPlayers());
		}
	}

	public void updatePlayersLeft(int less) {
		for (Player p : Bukkit.getOnlinePlayers()) {
			updateScore(p, "players",
					T.t(GameMain.getPlugin(),BattlePlayer.getLanguage(p.getUniqueId()), "scoreboard-players")
							+ (mode.getGameMain().playersLeft() - 1) + "/" + mode.getGameMain().getTotalPlayers());
		}
	}

	private void addScore(Player player, String scoreId, int score, String text) {
		String part1 = text;
		String part2 = "";
		if (text.length() > 12) {
			int a = 12;
			while (text.substring(0, a).endsWith("§"))
				--a;
			part1 = text.substring(0, a);
			part2 = text.substring(a, text.length());
			if (!part2.startsWith("§"))
				for (int i = part1.length(); i > 0; i--) {
					if (part1.substring(i - 1, i).equals("§") && part1.substring(i, i + 1) != null) {
						part2 = part1.substring(i - 1, i + 1) + part2;
						break;
					}
				}
			if (!part2.startsWith("§"))
				part2 = "§f" + part2;
		}
		String id = "";
		for (int i = 0; i < (score + "").length(); i++)
			id = id + "§" + (score + "").charAt(i);
		ScoreboardAPI.addScoreOnObjectiveToPlayer(player, "main", scoreId, score, id, part1, part2);
	}

	private void updateScore(Player player, String scoreId, String text) {
		String part1 = text;
		String part2 = "";
		if (text.length() > 12) {
			int a = 12;
			while (text.substring(0, a).endsWith("§"))
				--a;
			part1 = text.substring(0, a);
			part2 = text.substring(a, text.length());
			if (!part2.startsWith("§"))
				for (int i = part1.length(); i > 0; i--) {
					if (part1.substring(i - 1, i).equals("§") && part1.substring(i, i + 1) != null) {
						part2 = part1.substring(i - 1, i + 1) + part2;
						break;
					}
				}
			if (!part2.startsWith("§"))
				part2 = "§f" + part2;
		}
		ScoreboardAPI.updateScoreOnObjectiveToPlayer(player, "main", scoreId, part1, part2);
	}

	@Override
	public void enableScoreboard(Player p) {
		p.getScoreboard().getObjective("main").setDisplaySlot(DisplaySlot.SIDEBAR);
	}

	public static void main(String[] args) {
		String text = "text";
		int score = 12;
		String part1 = text;
		String part2 = "";
		if (text.length() > 12) {
			int a = 12;
			while (text.substring(0, a).endsWith("§"))
				--a;
			part1 = text.substring(0, a);
			part2 = text.substring(a, text.length());
			if (!part2.startsWith("§"))
				for (int i = part1.length(); i > 0; i--) {
					if (part1.substring(i - 1, i).equals("§") && part1.substring(i, i + 1) != null) {
						part2 = part1.substring(i - 1, i + 1) + part2;
						break;
					}
				}
			if (!part2.startsWith("§"))
				part2 = "§f" + part2;
		}
		String id = "";
		for (int i = 0; i < (score + "").length(); i++)
			id = id + "§" + (score + "").charAt(i);
		System.out.println(id);
	}

}
