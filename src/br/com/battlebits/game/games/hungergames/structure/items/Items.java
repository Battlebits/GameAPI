package br.com.battlebits.game.games.hungergames.structure.items;

import java.util.List;

import org.bukkit.inventory.ItemStack;

public interface Items {
	public List<ItemStack> generateItems();
}
