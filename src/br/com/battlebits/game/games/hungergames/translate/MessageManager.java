package br.com.battlebits.game.games.hungergames.translate;

import java.util.Random;

import br.com.battlebits.commons.core.translate.T;
import br.com.battlebits.game.GameMain;
import org.bukkit.Location;

import br.com.battlebits.commons.core.translate.Language;
import br.com.battlebits.commons.core.translate.Translate;
import br.com.battlebits.commons.util.DateUtils;

public class MessageManager {

	public static String getPlayersMessage(Language lang, int time) {
		String message = T.t(GameMain.getPlugin(),lang, "game-start");
		message = message.replace("%time%", DateUtils.formatDifference(lang, time));
		return message;
	}

	public static String getCountDownMessage(CountDownMessageType type, Language lang, int time) {
		return getCountDownMessage(type, lang, time, null);
	}

	public static String getCountDownMessage(CountDownMessageType type, Language lang, int time, Location loc) {
		String message = T.t(GameMain.getPlugin(),lang, type.toString().toLowerCase() + "-timer");
		message = message.replace("%time%", DateUtils.formatDifference(lang, time));
		if (loc != null) {
			message = message.replace("%x%", loc.getBlockX() + "");
			message = message.replace("%y%", loc.getBlockY() + "");
			message = message.replace("%z%", loc.getBlockZ() + "");
		}
		return message;
	}

	public static String getMinifeastMessage(Language lang, Location loc) {
		String message = T.t(GameMain.getPlugin(),lang, "minifeast-spawned");
		int x = loc.getBlockX();
		int z = loc.getBlockZ();
		while (x % 50 != 0) {
			x++;
		}
		while (z % 50 != 0) {
			z++;
		}
		int x2 = x;
		int z2 = z;
		Random r = new Random();
		if (r.nextBoolean()) {
			x += 50;
			x2 -= 50;
		} else {
			x -= 50;
			x2 += 50;
		}

		if (r.nextBoolean()) {
			z += 50;
			z2 -= 50;
		} else {
			z -= 50;
			z2 += 50;
		}
		message = message.replace("%x1%", "" + x);
		message = message.replace("%x2%", "" + x2);
		message = message.replace("%z1%", "" + z);
		message = message.replace("%z2%", "" + z2);
		return message;
	}

	public static enum CountDownMessageType {
		GAMESTART, INVINCIBILITY, FEAST, FEAST_SPAWNED, FINALARENA, FINISH;
	}

}
