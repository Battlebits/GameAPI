package br.com.battlebits.game.games.hungergames.util;

import java.util.ArrayList;
import java.util.Random;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class GiftGenerator {

	public static ItemStack generateGift() {
		ItemStack clay = new ItemStack(Material.STAINED_CLAY, 1, (short) new Random().nextInt(15));
		ItemMeta clayMeta = clay.getItemMeta();
		clayMeta.setDisplayName("%msg:giftDisplay%");
		ArrayList<String> lore = new ArrayList<>();
		lore.add("%msg:giftLore1%");
		lore.add("%msg:giftLore1%");
		clayMeta.setLore(lore);
		clay.setItemMeta(clayMeta);
		return clay;
	}
}
