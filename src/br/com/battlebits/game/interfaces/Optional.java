package br.com.battlebits.game.interfaces;

import br.com.battlebits.game.constructor.CustomOption;

public interface Optional {
	public CustomOption getOption(String abilityName, String optionName);

	public void setOption(String abilityName, String optionName, CustomOption value);
}
