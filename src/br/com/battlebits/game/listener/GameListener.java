package br.com.battlebits.game.listener;

import org.bukkit.event.Listener;

import br.com.battlebits.game.GameMain;
import br.com.battlebits.game.stage.GameStage;

public abstract class GameListener implements Listener {

	private GameMain gameMain;

	public GameListener(GameMain main) {
		this.gameMain = main;
	}

	public GameMain getGameMain() {
		return gameMain;
	}

	public GameStage getStage() {
		return gameMain.getGameStage();
	}
}
