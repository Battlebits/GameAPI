package br.com.battlebits.game.listener;

import br.com.battlebits.commons.core.translate.T;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.scoreboard.DisplaySlot;

import br.com.battlebits.commons.bukkit.scoreboard.ScoreboardAPI;
import br.com.battlebits.commons.core.account.BattlePlayer;
import br.com.battlebits.commons.core.permission.Group;
import br.com.battlebits.commons.core.translate.Translate;
import br.com.battlebits.game.GameMain;
import br.com.battlebits.game.constructor.CustomKit;
import br.com.battlebits.game.constructor.Gamer;
import br.com.battlebits.game.data.DataGamer;
import br.com.battlebits.game.gameevents.gamer.GamerJoinEvent;

public class JoinListener extends GameListener {

	public JoinListener(GameMain main) {
		super(main);
	}

	@EventHandler
	public void onAsyncPreLogin(AsyncPlayerPreLoginEvent event) {
		Gamer gamer = DataGamer.createIfNotExistMongo(event.getUniqueId());
		for (CustomKit kit : gamer.getAllCustomKits()) {
			kit.updateAbilities();
		}
		getGameMain().getGamerManager().addGamer(gamer);
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onLogin(PlayerLoginEvent event) {
		if (event.getResult() == Result.KICK_FULL || GameMain.getPlugin().playersLeft() >= 100) {
			BattlePlayer player = BattlePlayer.getPlayer(event.getPlayer().getUniqueId());
			if (player.hasGroupPermission(Group.LIGHT)) {
				event.allow();
			} else {
				event.disallow(Result.KICK_FULL, T.t(GameMain.getPlugin(), player.getLanguage(), "server-full"));
			}
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onJoin(PlayerJoinEvent event) {
		getGameMain().getGameEventManager().newEvent(new GamerJoinEvent(event.getPlayer().getUniqueId()));
		ScoreboardAPI.createObjectiveIfNotExistsToPlayer(event.getPlayer(), "clear", "", DisplaySlot.SIDEBAR);
	}
}
