package br.com.battlebits.game.manager;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import br.com.battlebits.commons.core.translate.T;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import br.com.battlebits.commons.bukkit.event.update.UpdateEvent;
import br.com.battlebits.commons.bukkit.event.update.UpdateEvent.UpdateType;
import br.com.battlebits.commons.core.account.BattlePlayer;
import br.com.battlebits.commons.core.translate.Language;
import br.com.battlebits.commons.core.translate.Translate;
import br.com.battlebits.commons.util.DateUtils;
import br.com.battlebits.game.GameMain;

public class CooldownManager {

	private HashMap<UUID, Map<String, Long>> cooldowns;

	public CooldownManager(GameMain main) {
		cooldowns = new HashMap<>();
		main.getServer().getPluginManager().registerEvents(new Listener() {

			@EventHandler
			public void onUpdate(UpdateEvent event) {
				if (event.getType() != UpdateType.SECOND)
					return;

				Iterator<UUID> it = cooldowns.keySet().iterator();
				while (it.hasNext()) {
					UUID uuid = it.next();
					Map<String, Long> map = cooldowns.get(uuid);
					Iterator<Entry<String, Long>> entryIterator = map.entrySet().iterator();
					while (entryIterator.hasNext()) {
						Entry<String, Long> entry = entryIterator.next();
						if (System.currentTimeMillis() >= entry.getValue()) {
							Player t = Bukkit.getPlayer(uuid);
							if (t != null) {
								t.playSound(t.getLocation(), Sound.LEVEL_UP, 0.5F, 1.0F);
							}
							entryIterator.remove();
						}
					}
				}
			}

		}, main);
	}

	public void removeAllCooldowns(UUID id) {
		cooldowns.remove(id);
	}

	public boolean hasCooldown(UUID id, String key) {
		if (cooldowns.containsKey(id) && cooldowns.get(id).containsKey(key)) {
			return true;
		}
		return false;
	}

	public void removeCooldown(UUID id, String key) {
		if (hasCooldown(id, key)) {
			if (cooldowns.containsKey(id))
				cooldowns.get(id).remove(key);
		}
	}

	public void setCooldown(UUID id, String key, int seconds) {
		Map<String, Long> timings = cooldowns.get(id);
		if (timings == null)
			timings = new HashMap<>();
		timings.put(key, System.currentTimeMillis() + (seconds * 1000));
		cooldowns.put(id, timings);
	}

	public boolean isOnCooldown(UUID id, String key) {
		if (hasCooldown(id, key)) {
			if (System.currentTimeMillis() - getLongTime(id, key) >= 0) {
				removeCooldown(id, key);
				return false;
			}
			return true;
		}
		return false;
	}

	public String getCooldownTimeFormated(UUID id, String key) {
		return getCooldownTimeFormated(BattlePlayer.getLanguage(id), id, key);
	}

	public String getCooldownFormated(UUID id, String key) {
		Language lang = BattlePlayer.getLanguage(id);
		return T.t(GameMain.getPlugin(), lang, "cooldown-message").replace("%tag%", key.toUpperCase()).replace("%time%", getCooldownTimeFormated(BattlePlayer.getLanguage(id), id, key));
	}

	public String getCooldownTimeFormated(Language lang, UUID id, String key) {
		if (isOnCooldown(id, key)) {
			return DateUtils.formatDifference(lang, (int) ((getLongTime(id, key) - System.currentTimeMillis()) / 1000));
		} else {
			return "1 segundo";
		}
	}

	private Long getLongTime(UUID id, String key) {
		if (hasCooldown(id, key)) {
			return cooldowns.get(id).get(key);
		} else {
			return System.currentTimeMillis();
		}
	}

}
