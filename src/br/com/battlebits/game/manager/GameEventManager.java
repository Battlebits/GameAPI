package br.com.battlebits.game.manager;

import java.util.ArrayList;
import java.util.List;

import br.com.battlebits.game.gameevents.GameEvent;
import br.com.battlebits.game.gameevents.GameEventType;

public class GameEventManager {
	private List<GameEvent> events;

	public GameEventManager() {
		events = new ArrayList<>();
	}

	public void newEvent(GameEventType type) {
		newEvent(type.name().toLowerCase(), type);
	}

	public void newEvent(String name, GameEventType type) {
		newEvent(new GameEvent(name, type));
	}

	public void newEvent(GameEvent event) {
		events.add(event);
	}

}
