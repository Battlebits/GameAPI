package br.com.battlebits.game.manager;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import br.com.battlebits.game.GameMain;
import br.com.battlebits.game.constructor.Kit;
import br.com.battlebits.game.event.player.PlayerSelectKitEvent;
import br.com.battlebits.game.event.player.PlayerSelectedKitEvent;
import br.com.battlebits.game.util.ClassGetter;

public class KitManager {

	private static HashMap<UUID, Kit> playersCurrentKit = new HashMap<>();

	private static HashMap<String, Kit> kits = new HashMap<>();

	public KitManager() {
		initializateKits("br.com.battlebits.game.games.hungergames.kit");
	}

	public void initializateKits(String packageName) {
		int i = 0;
		for (Class<?> abilityClass : ClassGetter.getClassesForPackage(GameMain.getPlugin(), packageName)) {
			if (Kit.class.isAssignableFrom(abilityClass)) {
				try {
					Kit abilityListener;
					try {
						abilityListener = (Kit) abilityClass.getConstructor(GameMain.class).newInstance(GameMain.getPlugin());
					} catch (Exception e) {
						abilityListener = (Kit) abilityClass.newInstance();
					}
					String kitName = abilityListener.getClass().getSimpleName().toLowerCase().replace("kit", "");
					kits.put(kitName, abilityListener);
				} catch (Exception e) {
					e.printStackTrace();
					System.out.print("Erro ao carregar o kit " + abilityClass.getSimpleName());
				}
				i++;
			}
		}
		GameMain.getPlugin().getLogger().info(i + " kits carregados!");
	}

	public static HashMap<String, Kit> getKits() {
		return kits;
	}

	public static Kit getKit(String kitName) {
		if (kits.containsKey(kitName.toLowerCase()))
			return kits.get(kitName.toLowerCase());
		else
			System.out.print("Tried to find ability '" + kitName + "' but failed!");
		return null;
	}

	public Kit getPlayerKit(Player player) {
		return playersCurrentKit.containsKey(player.getUniqueId()) ? playersCurrentKit.get(player.getUniqueId()) : null;
	}

	public Kit getPlayerKit(UUID uuid) {
		return playersCurrentKit.containsKey(uuid) ? playersCurrentKit.get(uuid) : null;
	}

	public void selectKit(Player player, Kit kit) {
		PlayerSelectKitEvent event = new PlayerSelectKitEvent(player, kit);
		Bukkit.getPluginManager().callEvent(event);
		if (!event.isCancelled()) {
			GameMain.getPlugin().getAbilityManager().unregisterPlayer(player);
			kit.loadAbilities(player);
			playersCurrentKit.put(player.getUniqueId(), kit);
			Bukkit.getPluginManager().callEvent(new PlayerSelectedKitEvent(player, kit));
		}
	}

	public void unregisterPlayer(Player player) {
		GameMain.getPlugin().getAbilityManager().unregisterPlayer(player);
		playersCurrentKit.remove(player.getUniqueId());
		Bukkit.getPluginManager().callEvent(new PlayerSelectedKitEvent(player, null));
	}

}
