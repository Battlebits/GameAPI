package br.com.battlebits.game.scheduler;

import br.com.battlebits.game.constructor.ScheduleArgs;

public interface Schedule {

	public void pulse(ScheduleArgs args);
}
