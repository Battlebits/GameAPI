package br.com.battlebits.game.scheduler;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import br.com.battlebits.commons.bukkit.event.update.UpdateEvent;
import br.com.battlebits.commons.bukkit.event.update.UpdateEvent.UpdateType;
import br.com.battlebits.game.GameMain;

public class SchedulerListener implements Listener {

	private GameMain main;

	public SchedulerListener(GameMain main) {
		this.main = main;
	}

	@EventHandler
	public void onUpdate(UpdateEvent event) {
		if (event.getType() != UpdateType.SECOND)
			return;
		main.getSchedulerManager().pulse();
		main.count();
	}

}
