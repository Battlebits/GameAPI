package br.com.battlebits.game.stage;

public enum CounterType {
	COUNTDOWN, COUNT_UP, STOP
}
